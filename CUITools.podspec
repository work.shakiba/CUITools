#
# Be sure to run `pod lib lint CUITools.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CUITools'
  s.version          = '0.8.1'
  s.summary          = 'UI tools for internal usage'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
UI tools for usage in internal apps.
including loading, select and more
                       DESC

  s.homepage         = 'https://github.com/mohsenShakiba/CUITools'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mohsenShakiba' => 'work.shakiba@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/work.shakiba/CUITools.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'CUITools/Classes/**/*'
  
  s.resource_bundles = {
    'CUITools' => ['CUITools/Assets/*.ttf']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
s.dependency 'SwiftHTTP'
s.dependency 'AdvancedTableView'
s.dependency 'SnapKit', '< 4.0.0'
s.dependency 'DKImagePickerController', '< 3.8.0'
s.dependency 'SelectController'
s.dependency 'ImageViewer', '~> 4.0'

end
