//
//  LoadingViewController.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 7/28/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class MSTableViewControllerTest: MSTableViewController {
    
    var items: [Int] = []
    
    override var shouldEnablePullToRefresh: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        view.backgroundColor = .white
        showLoading()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.onResult()
        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
//            self.onError()
//        }
    }
    
    fileprivate func onError() {
        self.showError(message: "an error accured", image: nil)
    }
    
    fileprivate func onResult() {
        for i in 0...19 {
            items.append(i)
        }
        self.showTableView()
        tableView.reloadData()
    }
    
    override func refreshDidBegin() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.endRefresh()
        }
    }
    
}

extension MSTableViewControllerTest: UITableViewDelegate {
    
}

extension MSTableViewControllerTest: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = String(indexPath.row)
        return cell
    }
    
}

