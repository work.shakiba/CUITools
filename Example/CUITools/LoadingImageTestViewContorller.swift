//
//  LoadingImageTestViewContorller.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 7/30/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class LoadingImageViewTestCell: UITableViewCell {
    
    let loadingImageView = UIImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(loadingImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class LoadingImageViewTestViewController: UITableViewController {
    
    var images = [
        "https://cdn.dribbble.com/users/418188/screenshots/3491912/behance_portfolio_reviews_dnipro_tubik.png",
        "http://www.apple.com/euro/ios/ios8/a/generic/images/og.png",
        "https://cdn.dribbble.com/users/191949/screenshots/3492529/cc_vampire_swampcreature_stickers.jpg",
        "https://cdn.dribbble.com/users/692322/screenshots/3496148/dribbble.png",
        "https://cdn.dribbble.com/users/347174/screenshots/3495955/attachments/772207/2x.png",
        "https://cdn.dribbble.com/users/1715481/screenshots/3496258/dribbble_1st_shot_ines.png",
        //        "https://cdn.dribbble.com/users/879147/screenshots/3496621/untitled-3.jpg"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        tableView.register(LoadingImageViewTestCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 999
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! LoadingImageViewTestCell
        let index = indexPath.row % images.count
        cell.loadingImageView.url = images[index]
        cell.loadingImageView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116
    }
    
}
