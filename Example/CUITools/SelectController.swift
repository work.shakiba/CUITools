//
//  File.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 8/23/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class SelectController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addSelectOptionButton()
        addPickerButton()
    }
    
    fileprivate func addSelectOptionButton() {
        let button = UIButton()
        view.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(self.view)
            make.height.equalTo(100)
        }
        button.setTitleColor(.black, for: .normal)
        button.setTitle("touch to presnet", for: .normal)
        button.addTarget(self, action: #selector(onButtonTapped), for: .touchUpInside)
    }
    
    fileprivate func addPickerButton() {
        let button = UIButton()
        view.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.top.equalTo(100)
            make.left.right.equalTo(self.view)
            make.height.equalTo(100)
        }
        button.setTitleColor(.black, for: .normal)
        button.setTitle("touch to presnet picker", for: .normal)
        button.addTarget(self, action: #selector(onPickerClicked), for: .touchUpInside)
    }
    
    func onButtonTapped() {
        let provider = SelectOptionProvider<Int>(cancelTitle: "cancel")
        provider.appendItem(title: "item 1", value: 1)
        provider.appendItem(title: "item 2", value: 2)
        provider.appendItem(title: "item 3", value: 3)
        provider.onResult { (result) in
            switch result {
            case .cancel:
                print("cancel")
                break
            case .result(let result):
                print("value", result)
                break
            }
        }
        provider.present(in: self)
    }
    
    func onPickerClicked() {
        let provider = PickerProvider(headerTitle: "this is the title", cancelTitle: "cancelll")
        provider.addItem("one", value: 1, component: 0)
        provider.addItem("two", value: 2, component: 0)
        provider.addItem("three", value: 2, component: 0)
        provider.addItem("one", value: 1, component: 1)
        provider.addItem("two", value: 2, component: 2)
        provider.addItem("three", value: 2, component: 3)
        provider.onResult { (result) in
            switch result {
            case .cancel:
                print("cancel")
                break
            case .result(let result):
                print("value", result)
                break
            }
        }
        provider.present(in: self)
    }
    
}
