//
//  NotificationViewController.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 8/17/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class NotificationViewController: UIViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func notification(_ sender: Any) {
        let provider = NotificationProvider(text: "this is a test")
        provider.present(in: self)
    }
    
    @IBAction func notificationInWindow(_ sender: Any) {
        let provider = NotificationProvider(text: "this is another test")
        provider.presentInWindow()
    }
    
}
