//
//  MSViewController.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 7/27/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class MSViewTestController: MSViewController {

    
    var initView: UIView {
        let view = UIView()
        view.backgroundColor = UIColor.red
        return view
    }
    
    var finalView: UIView {
        let view = UIView()
        view.backgroundColor = UIColor.blue
        return view
    }
    
    override var initialView: UIView {
        return initView
    }
    
    var isInitView: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        msView.delegate = self
        let button = UIButton()
        button.setTitle("transit to next to view", for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: 50)
        button.addTarget(self, action: #selector(transitTo), for: .touchUpInside)
        view.addSubview(button)
        
    }
    
    @objc func transitTo() {
        if isInitView {
            msView.transit(to: finalView)
        }else{
            msView.transit(to: initView)
        }
        isInitView = !isInitView
    }
    

    
}

extension MSViewTestController: MSViewDelegate {
    
    func msView(viewAddedToSuperView view: UIView, superView: UIView) {
        view.frame = superView.bounds
    }
    
}
