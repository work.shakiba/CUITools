//
//  ViewController.swift
//  CUITools
//
//  Created by mohsenShakiba on 10/18/2017.
//  Copyright (c) 2017 mohsenShakiba. All rights reserved.
//

import UIKit
import CUITools


class ViewController: UITableViewController {
    
    internal enum Types: Int {
        case successText = 0
        case success = 1
        case errorText = 2
        case error = 3
        case customText = 4
        case msView = 5
        case loadingView = 6
        case errorView = 7
        case viewControllerExtension = 8
        case loadingButtonTest = 9
        case loadingImageView = 10
        case selectImage = 11
        case selectTest = 12
        case skeletonView = 13
        case formView = 14
        case imageViewer = 15
        case notification = 16
        case bulletin = 17

        static var count: Int {
            return 18
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Styles.global._accentColor = UIColor(hex: "00a3d9")
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Types.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        let title = String(reflecting: Types(rawValue: indexPath.row)!).replacingOccurrences(of: "CUITools_Example.ViewController.Types.", with: "")
        print("title", title)
        cell.textLabel?.text = title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let type = Types(rawValue: indexPath.row) else {
            return
        }
        switch type {
        case .successText: successtext()
        case .success: success()
        case .errorText: errorText()
        case .error: error()
        case .customText: customText()
        case .msView: msView()
        case .loadingView: loadingView()
        case .errorView: errorView()
        case .viewControllerExtension: viewControllerExtension()
        case .loadingButtonTest: loadingButtonTest()
        case .loadingImageView: loadingImageView()
        case .selectImage: selectImage()
        case .selectTest: selectTest()
        case .skeletonView: skeletonView()
        case .formView: formView()
        case .imageViewer: imageViewer()
        case .notification: notification()
        case .bulletin: bulletinTest()
        }
    }

    func successtext() {
        let provider = FlashViewProvider(success: "success")
        provider.present(in: self)
    }
    
    func success() {
        let provider = FlashViewProvider(success: nil)
        provider.present(in: self)
    }
    
    func errorText() {
        let provider = FlashViewProvider(error: "error")
        provider.present(in: self)
    }
    
    func error() {
        let provider = FlashViewProvider(error: nil)
        provider.present(in: self)
    }
    
    func customText() {
        let image = UIImage()
        let provider = FlashViewProvider(message: "hi", type: .custom(image))
        provider.present(in: self)
    }
    
    func msView() {
        let vc = MSViewTestController()
        self.present(vc, animated: true, completion: nil)
    }
    
    func loadingView() {
        let vc = LoadingViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
    func errorView() {
        let vc = MSTableViewControllerTest()
        self.present(vc, animated: true, completion: nil)
    }
    
    func viewControllerExtension() {
        let vc = UIViewControllerExtensionTest()
        self.present(vc, animated: true, completion: nil)
    }
    
    func loadingButtonTest() {
        let vc = LoadingButtonViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
    func loadingImageView() {
        let vc = LoadingImageViewTestViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
    func selectImage() {
        let vc = SelectImageTestViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
    func searchView() {
//        let vc = SearchViewTestController()
//        let nav = UINavigationController(rootViewController: vc)
//        self.present(nav, animated: true, completion: nil)
    }
    
    func skeletonView() {
        let vc = SekeletonViewTestController()
        self.present(vc, animated: true, completion: nil)
    }
    
    func formView() {
        let vc = FormViewTestViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
    func imageViewer() {
        let vc = ImageViewereTestController()
        self.present(vc, animated: true, completion: nil)
    }
    
    func notification() {
        let vc = NotificationViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
    func selectTest() {
        let vc = SelectController()
        self.present(vc, animated: true, completion: nil)
    }
    
    func bulletinTest() {
        let vc = BulletinTestViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
}

