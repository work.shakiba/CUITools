//
//  SelectImageTestViewController.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 8/1/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class SelectImageTestViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        let button = UIButton()
        button.setTitle("select to present image view", for: .normal)
        button.sizeToFit()
        button.center = view.center
        button.setTitleColor(.red, for: .normal)
        view.addSubview(button)
        button.addTarget(self, action: #selector(onImageSet), for: .touchUpInside)
        
    }
    
    @objc func onImageSet() {
        var config = SelectImageConfig()
        config.selectionCount = 5
        self.presentSelectImage(config: config)
    }
    
    
}

extension SelectImageTestViewController: SelectImageProtocol {
    
    func imageSelectionSucceed(images: [UIImage]) {
        print("count of image", images.count)
    }
    
    func imageSelectionCanceled() {
        
    }
    
}
