//
//  UIViewControllerExtensionTest.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 7/29/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class UIViewControllerExtensionTest: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.startLoading()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.stopLoading()
        }
    }
    
}
