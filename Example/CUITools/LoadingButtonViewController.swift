//
//  LoadingButtonViewController.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 7/30/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class LoadingButtonViewController: UIViewController {
    
    var loadingButton = LoadingButton(title: "signup for good")
    var loadingButtonBorder = LoadingButton(title: "signup for good border")
    var loadingButtonMinimal = LoadingButton(title: "signup for good border")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        view.addSubview(loadingButton)
        loadingButton.translatesAutoresizingMaskIntoConstraints = false
        loadingButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        loadingButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 64).isActive = true
        
        loadingButton.addTarget(self, action: #selector(buttonBackgroundAction), for: .touchUpInside)
        
        loadingButtonBorder.isEnabled = false
        loadingButtonBorder.type = 1
        view.addSubview(loadingButtonBorder)
        loadingButtonBorder.translatesAutoresizingMaskIntoConstraints = false
        loadingButtonBorder.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        loadingButtonBorder.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 120).isActive = true
        
        loadingButtonBorder.addTarget(self, action: #selector(buttonBorderBackgroundAction), for: .touchUpInside)
        
        loadingButtonMinimal.isEnabled = false
        loadingButtonMinimal.type = 2
        view.addSubview(loadingButtonMinimal)
        loadingButtonMinimal.translatesAutoresizingMaskIntoConstraints = false
        loadingButtonMinimal.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        loadingButtonMinimal.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 200).isActive = true
        
        loadingButtonMinimal.addTarget(self, action: #selector(buttonBorderMinimaldAction), for: .touchUpInside)
        
    }
    
    @objc func buttonBackgroundAction() {
        loadingButtonMinimal.isEnabled = true
        loadingButtonBorder.isEnabled = true
        loadingButton.isLoading = !loadingButton.isLoading
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.loadingButton.showError()
        }
    }
    
    @objc func buttonBorderBackgroundAction() {
        loadingButtonBorder.isLoading = !loadingButtonBorder.isLoading
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.loadingButtonBorder.showError()
        }
    }
    
    @objc func buttonBorderMinimaldAction() {
        loadingButtonMinimal.isLoading = !loadingButtonMinimal.isLoading
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.loadingButtonMinimal.showError()
        }
    }
    
}
