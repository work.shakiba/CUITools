//
//  FormViewTestViewController.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 8/3/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class FormViewTestViewController: FormViewController {
    
    
    let firstNameFormView = TextFormView(title: "First name", value: nil)
    let lastNameFormView = TextFormView(title: "Last name", value: nil)
    let genderFormView = SelectFormView(title: "gender", value: "Unselected")

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        formView.add(form: firstNameFormView)
        formView.add(form: lastNameFormView)
        formView.add(form: genderFormView)
        let formButton = FormButtonView(title: "send data")
        formButton.addTarget(self, action: #selector(onAction), for: .touchUpInside)
        formView.add(accessory: formButton)
        
        genderFormView.addTarget(self, action: #selector(onAction), for: .touchUpInside)
        
    }
    
    @objc func onAction() {
        genderFormView.reload { (view) in
            view.value = "gender"
        }
    }
    
}
