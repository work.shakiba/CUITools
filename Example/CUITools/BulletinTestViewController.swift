//
//  File.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 8/23/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class BulletinTestViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addBulletinButton()
    }
    
    fileprivate func addBulletinButton() {
        let button = UIButton()
        view.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.left.right.top.equalTo(self.view)
            make.height.equalTo(100)
        }
        button.setTitleColor(.black, for: .normal)
        button.setTitle("touch to presnet", for: .normal)
        button.addTarget(self, action: #selector(onButtonTapped), for: .touchUpInside)
    }
    
    func onButtonTapped() {
        let provider = BulletinAlertProvider(title: "this is title", detail: "this is detail", image: nil, actionTitle: "this is action", cancelTitle: "this is cancel")
        provider.present(in: self)
    }
    
}

