//
//  SkeletonViewTestController.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 8/3/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class SekeletonViewTestController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        let definition = SkeletonViewDefinition()
        definition.addSubview(offset: 0, type: .image, direction: .left)
        definition.addSubview(offset: 3, type: .text, direction: .left)
        definition.goToNextLine()
        definition.addSubview(offset: 3, type: .text, direction: .left)
        definition.goToNextLine()
        definition.addSubview(offset: 0, type: .text, direction: .left)
        let skeletonView = SkeletonView(frame: self.view.bounds, definition: definition)
        view.addSubview(skeletonView)
    }
    
}
