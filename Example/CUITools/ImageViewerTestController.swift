//
//  ImageViewerTestController.swift
//  CUITools_Example
//
//  Created by mohsen shakiba on 8/6/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import CUITools

class ImageViewereTestController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image = #imageLiteral(resourceName: "ghost")
        let imageView = UIImageView()
        imageView.image = image
        imageView.sizeToFit()
        view.addSubview(imageView)
        imageView.center = self.view.center
        
        let tapGestureRecognize = UITapGestureRecognizer(target: self, action: #selector(onImageTap))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognize)
    }
    
    @objc func onImageTap() {
        self.presentImage(image: #imageLiteral(resourceName: "ghost"))
    }
    
}

extension ImageViewereTestController: ImageViewerExtension {
    
    
}
