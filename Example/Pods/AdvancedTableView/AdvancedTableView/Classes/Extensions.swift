//
//  UINibExtenion.swift
//  Pods
//
//  Created by mohsen shakiba on 12/30/1395 AP.
//
//

import Foundation
import UIKit

extension UINib {
    
    static func fromType(type: AnyClass, bundle: Bundle) -> UINib {
        let nibName = name(type: type)
        return UINib(nibName: nibName, bundle: bundle)
    }
    
    static func name(type: AnyClass) -> String {
        let nibName = NSStringFromClass(type).components(separatedBy: ".").last
        return nibName!
    }
    
}

extension String {
    
    static func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
}
