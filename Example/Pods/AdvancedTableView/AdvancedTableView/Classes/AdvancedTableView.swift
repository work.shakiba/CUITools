//
//  ListViewController.swift
//  Pods
//
//  Created by mohsen shakiba on 12/12/1395 AP.
//
//

import UIKit

public class AdvancedTableView: UIView {
    
    public private(set) var tableView: UITableView!
    public weak var delegate: AdvancedTableViewDelegate? {
        didSet {
            onDelegateSet()
        }
    }
    public var dataSource: DataSource!
    var style: UITableViewStyle
    var isUpdating = false
    
    public var backgroundView: UIView? {
        set {
            tableView.backgroundView = newValue
        }
        get{
            return tableView.backgroundView
        }
    }
    
    public var headerView: UIView? {
        get {
            return tableView.tableHeaderView
        }
        set {
            tableView.tableHeaderView = newValue
        }
    }
    
    public var footerView: UIView? {
        get {
            return tableView.tableFooterView
        }
        set {
            tableView.tableFooterView = newValue
        }
    }
    
    public init(frame: CGRect, style: UITableViewStyle) {
        self.style = style
        super.init(frame: frame)
        dataSource = DataSource(self)
        setupTableView()
    }
    
    private func onDelegateSet() {
        self.beginUpdates()
        delegate?.setupTableView(dataSource: dataSource)
        self.endUpdates()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        self.style = .plain
        super.init(coder: aDecoder)
        dataSource = DataSource(self)
    }
    
    private func setupTableView() {
        tableView = UITableView(frame: frame, style: style)
        addSubview(tableView)
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
    }
    
    public var keyboardDismissMode: UIScrollViewKeyboardDismissMode = UIScrollViewKeyboardDismissMode.interactive {
        didSet {
            tableView.keyboardDismissMode = keyboardDismissMode
        }
    }
    
    public func beginUpdates() {
        isUpdating = true
        tableView.beginUpdates()
    }
    
    public func endUpdates() {
        tableView.endUpdates()
        isUpdating = false
    }
    
    func beginUpdatesIfNeeded() {
        if isUpdating { return }
        tableView.beginUpdates()
    }
    
    func endUpdatesIfNeeded() {
        if isUpdating { return }
        tableView.endUpdates()
    }
    
    public func reload() {
        UIView.setAnimationsEnabled(false)
        dataSource.clear()
        beginUpdatesIfNeeded()
        delegate?.setupTableView(dataSource: dataSource)
        endUpdatesIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    public var separetorInset: UIEdgeInsets {
        get {
            return self.tableView.separatorInset
        }
        set {
            self.tableView.separatorInset = newValue
        }
    }
    
    public var separetorColor: UIColor? {
        get {
            return self.tableView.separatorColor
        }
        set {
            self.tableView.separatorColor = newValue
        }
    }
    
    public var separetorStyle: UITableViewCellSeparatorStyle {
        get {
            return self.tableView.separatorStyle
        }
        set {
            self.tableView.separatorStyle = newValue
        }
    }
    
    public var contentInsets: UIEdgeInsets {
        get {
            return tableView.contentInset
        }
        set {
            tableView.contentInset = newValue
        }
    }
    
    var didLoad = false
    public override func layoutSubviews() {
        super.layoutSubviews()
        if didLoad { return }
        didLoad = true
    }
    
    public func section(by index: Int) -> Section {
        return dataSource.section(by: index)
    }
    
    public func section(by id: String) -> Section {
        return dataSource.section(by: id)
    }
    
}

public protocol AdvancedTableViewDelegate: class {
    
    func setupTableView(dataSource: DataSource)
    
}

public class DataSource: NSObject {
    
    var sections: [Section] = []
    weak var advancedTableView: AdvancedTableView?
    
    init(_ advancedTableView: AdvancedTableView) {
        self.advancedTableView = advancedTableView
    }
    
    public func section(id: String, _ handler: (Section) -> Void) {
        let section = Section(advancedTableView!, id: id)
        let indexSet = IndexSet(integer: sections.count)
        sections.append(section)
        advancedTableView?.tableView.insertSections(indexSet, with: .none)
        reindex()
        handler(section)
        section.setupDidFinish()
    }
    
    public func section(id: String) {
        self.section(id: id) { (s) in
        }
    }
    
    func reindex() {
        for (i, s) in sections.enumerated() {
            s.index = i
        }
    }
    
    func section(by id: String) -> Section {
        return sections.first(where: {$0.id == id})!
    }
    
    func section(by index: Int) -> Section {
        return sections[index]
    }
    
    public func clear() {
        self.sections.removeAll()
        self.advancedTableView?.tableView.reloadData()
    }
    
}

extension DataSource: UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sections[section]
        return section.cellCount
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        let cell = section.cell(for: indexPath.row)
        return cell
    }
    
}

extension DataSource: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        section.cell(event: .selected, for: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        section.cell(event: .deSelected, for: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        section.cell(event: .willDisplay, for: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let section = sections[indexPath.section]
//        section.cell(event: .didEndDisplaying, for: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        section.cell(event: .highlight, for: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        section.cell(event: .unHighlight, for: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let section = sections[indexPath.section]
        return section.canEdit(for: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let section = sections[section]
        return section.footerView
    }

    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = sections[section]
        return section.headerView
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let section = sections[section]
        let height = section.heightForFooter()
        if tableView.style == .grouped {
            return height ?? UITableViewAutomaticDimension
        }else{
            return height ?? 0
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = sections[section]
        let height = section.heightForHeader()
        if tableView.style == .grouped {
            return height ?? UITableViewAutomaticDimension
        }else{
            return height ?? 0
        }
    }
    
    public func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let section = sections[indexPath.section]
        return section.shouldHighlight(row: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let section = sections[indexPath.section]
        return section.editingActions(row: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = sections[indexPath.section]
        return section.rowHeight(for: indexPath.row)
    }
    
}
