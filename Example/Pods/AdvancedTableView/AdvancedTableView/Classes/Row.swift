//
//  Row.swift
//  Pods
//
//  Created by mohsen shakiba on 3/25/1396 AP.
//
//

// reload with closure
// reload
//

// better dynamic section delegate naming
// view for empty status

import UIKit


open class Row<T>: BaseRow, PrivateRowProtocol where T: UITableViewCell {
    
    public var cell: T? {
        return innerCell as? T
    }
    
    public override init() {}
    
    open func setup(cell: T) {}
    
    /// methods to be overriden
    open func row(selected cell: T) {}
    open func row(deselected cell: T) {}
    open func row(highlighted cell: T) {}
    open func row(unhighlighted cell: T) {}
    open func row(disappeared cell: T) {}
    
    public override func add(to: Section) {
        setupID()
        setupNib()
        section = to
        to.add(row: self, at: self.index)
    }
    
    internal func setupID() {
        if id.isEmpty {
            id = String.randomString(length: 10)
        }
    }
    
    internal func setupNib() {
        let bundle = Bundle(for: T.classForCoder())
        let nib = UINib.fromType(type: T.self, bundle: bundle)
        let nibName = UINib.name(type: T.self)
        self.reuseIdentifier = nibName
        self.nib = nib
    }

    override func rowSelected() {
        super.rowSelected()
        self.row(selected: innerCell as! T)
    }
    
    override func rowDeselected() {
        self.row(deselected: innerCell as! T)
    }
    
    override func rowHighlighted() {
        self.row(highlighted: innerCell as! T)
    }
    
    override func rowUnhighlighted() {
        self.row(unhighlighted: innerCell as! T)
    }
    
    override func rowDisappeared() {
        self.row(disappeared: innerCell as! T)
    }
    
    override func setup() {
        self.setup(cell: innerCell as! T)
    }
        
}

open class BaseRow : NSObject, RowProtocol {

    
    public var shouldHighlight: Bool = true
    
    public func add(to: Section) {
        fatalError("cannot call append from here")
    }
    
    public var id: String = ""
    public var reuseIdentifier: String = ""
    public var nib: UINib!
    public var index: Int = -1
    public var innerCell: UITableViewCell!
    public weak var section: Section?
    public var rowActions: [UITableViewRowAction] = []
    
    public var clickHandler: (() -> Void)?
    public var updateHandler: ((RowProtocol) -> Void)?
    public var eventHandler: ((String, Any?) -> Void)?
    var editingActions: [UITableViewRowAction] = []
    public var preventUnHighlightingAfterSelection: Bool = false
    
    open func height() -> CGFloat {
        fatalError("height not overriden")
    }
    
    override init() {
    }
    
    open func cell(disappeared cell: UITableViewCell) {}
    open func cell(appeared cell: UITableViewCell) {}
    
    public func remove() {
        assert(section != nil)
        assert(index != -1)
        section?.remove(byIndex: self.index)
    }
    
    public func reload() {
        assert(section != nil)
        assert(index != -1)
        section?.reload(byIndex: self.index)
    }
    
    public func emit(event: RowCustomEvents) {
        switch event {
        case .click: self.rowSelected()
        case .custom(let event, let value):
            if let handler = self.eventHandler {
                handler(event, value)
            }
        }
        
    }
    
    public func id(_ id: String) -> Self {
        self.id = id
        return self
    }
    
    public func at(_ index: Int) -> Self {
        self.index = index
        return self
    }
    
    public func event(_ handler: @escaping (String, Any?) -> Void) -> Self {
        self.eventHandler = handler
        return self
    }
    
    public func click(_ handler: @escaping () -> Void) -> Self {
        clickHandler = handler
        return self
    }
    
    open func on(event: RowEvent) {
        switch event {
        case .highlight:
            self.rowHighlighted()
        case .unHighlight:
            self.rowUnhighlighted()
        case .selected:
            self.rowSelected()
        case .deSelected:
            self.rowDeselected()
        case .willDisplay:
            self.cellWillDisplay()
        case .didEndDisplaying:
            self.rowDisappeared()
        }
    }
    
    func cellWillDisplay() {
        updateHandler?(self)
        setup()
    }
    
    public func editingActions(_ handler: () -> [UITableViewRowAction]) -> Self {
        rowActions = handler()
        return self
    }
    
    func setup() {}
    func rowHighlighted() {}
    func rowDisappeared() {}
    func rowUnhighlighted() {}
    func rowDeselected() {}
    
    func rowSelected() {
        if let handler = self.clickHandler {
            handler()
        }
        let indexPath = IndexPath(item: index, section: section!.index)
        section?.advancedTableView?.tableView.deselectRow(at: indexPath, animated: true)
    }
}


protocol PrivateRowProtocol: class {
    
    func setupNib()
    func setupID()
    func setup()
}

public protocol RowProtocol: class {
    
    var id: String { get }
    var rowActions: [UITableViewRowAction] { get set }
    var innerCell: UITableViewCell! { get set }
    var index: Int { get set }
    var reuseIdentifier: String { get set }
    var nib: UINib! { get set }
    var section: Section? { get set }
    var clickHandler: (() -> Void)? { get set }
    var updateHandler: ((RowProtocol) -> Void)? { get set }
    var eventHandler: ((String, Any?) -> Void)? { get set }
    var shouldHighlight: Bool { get set }
    var preventUnHighlightingAfterSelection: Bool { get set }
    
    func add(to: Section)
    func height() -> CGFloat
    func on(event: RowEvent)
    func emit(event: RowCustomEvents)
    func click(_ handler: @escaping () -> Void) -> Self
    func event(_ handler: @escaping (String, Any?) -> Void) -> Self
    func editingActions(_ handler: () -> [UITableViewRowAction]) -> Self
    func at(_ index: Int) -> Self
    func id(_ id: String) -> Self
    
    func remove()
    func reload()
    
}

extension RowProtocol {
    
    public func define(_ handler: (Self) -> Void) -> Self {
        handler(self)
        return self
    }
    
    public func update(_ handler: @escaping (Self) -> Void) -> Self {
        self.updateHandler = {cell in
            handler(cell as! Self)
        }
        return self
    }
    
//    public func click<T>(_ handler: @escaping (T) -> () -> () ) -> Self {
//        let action = { [weak self] in
//            handler(self?.section?.controller as! T)()
//        }
//        clickHandler = action
//        return self
//    }
//    
//    public func event<T>(_ handler: @escaping (T) -> (String, Any?) -> () ) -> Self {
//        let action = { [weak self] (event, value)in
//            handler(self?.section?.controller as! T)(event, value)
//        }
//        eventHandler = action
//        return self
//    }
    
}

public enum RowCustomEvents {
    case click
    case custom(String, Any?)
}

public enum RowEvent {
    
    case highlight
    case unHighlight
    case selected
    case deSelected
    case willDisplay
    case didEndDisplaying
    
}
