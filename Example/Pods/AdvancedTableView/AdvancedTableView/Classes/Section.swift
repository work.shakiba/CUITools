//
//  Section.swift
//  Pods
//
//  Created by mohsen shakiba on 3/25/1396 AP.
//
//

import UIKit

public class Section {
    
    public var index: Int = -1
    
    weak var advancedTableView: AdvancedTableView?
    
    var rows: [RowProtocol] = []
    
    public var id: String
    
    public var headerView: UIView?
    public var footerView: UIView?
    public var headerHeight: CGFloat?
    public var footerHeight: CGFloat?
    
    public init(_ advancedTableView: AdvancedTableView, id: String) {
        self.advancedTableView = advancedTableView
        self.id = id
    }
    
    public var cellCount: Int {
        return rows.count
    }
    
    func setupDidFinish() {
        for row in rows {
            advancedTableView?.tableView.register(row.nib, forCellReuseIdentifier: row.reuseIdentifier)
        }
    }
    
    func cell(for row: Int) -> UITableViewCell {
        let row = rows[row]
        let identifier = row.reuseIdentifier
        let nib = row.nib
        advancedTableView?.tableView.register(nib, forCellReuseIdentifier: identifier)
        let cell = advancedTableView!.tableView.dequeueReusableCell(withIdentifier: identifier)
        row.innerCell = cell
        return cell!
    }
    
    func rowHeight(for row: Int) -> CGFloat {
        let row = rows[row]
        return row.height()
    }
    
    func rowActions(for row: Int) -> [UITableViewRowAction] {
        let row = self.rows[row]
        return row.rowActions
    }
    
    public func index(_ at: Int) -> Self {
        index = at
        return self
    }
    
    public func row(by id: String) -> RowProtocol? {
        return rows.first(where: {$0.id == id})
    }
    
    public func row(at index: Int) -> RowProtocol {
        return rows[index]
    }
    
    public func add(row: RowProtocol, at: Int?) {
        var index = at ?? rows.count
        if index == -1 {
            index = rows.count
        }
        advancedTableView?.beginUpdatesIfNeeded()
        rows.insert(row, at: index)
        let indexPath = IndexPath(item: index, section: self.index)
        advancedTableView?.tableView.insertRows(at: [indexPath], with: .fade)
        reIndex()
        advancedTableView?.endUpdatesIfNeeded()
    }
    
    public func remove(row: RowProtocol) {
        self.remove(byIndex: row.index)
    }
    
    public func remove(byId: String) {
        if let index = rows.index(where: {$0.id == byId}) {
            self.remove(byIndex: index)
        }
    }
    
    public func remove(byIndex: Int) {
        rows.remove(at: byIndex)
        advancedTableView?.beginUpdatesIfNeeded()
        let indexPath = IndexPath(item: byIndex, section: index)
        advancedTableView?.tableView.deleteRows(at: [indexPath], with: .none)
        reIndex()
        advancedTableView?.endUpdatesIfNeeded()
    }
    
    public func reload(byId: String) {
        if let index = rows.index(where: {$0.id == byId}) {
            self.reload(byIndex: index)
        }
    }
    
    public func reload(byIndex: Int) {
        advancedTableView?.beginUpdatesIfNeeded()
        rows.remove(at: byIndex)
        let indexPath = IndexPath(item: byIndex, section: index)
        advancedTableView?.tableView.reloadRows(at: [indexPath], with: .none)
        advancedTableView?.endUpdatesIfNeeded()
    }
    
    func canEdit(for row: Int) -> Bool {
        let row = rows[row]
        return row.rowActions.count > 0
    }
    
    // todo: fixed to mentionaed bug
    func cell(event: RowEvent, for row: Int) {
        // if row remove is the last row then didEndDisplaying causes indexOutOfRange
        // thus checking if index is in range
        if row >= rows.count { return }
        let row = rows[row]
        row.on(event: event)
    }
    
    public func clear() {
        self.rows.removeAll()
        let indexSet = NSIndexSet(index: self.index) as IndexSet
        self.advancedTableView?.tableView.reloadSections(indexSet, with: .fade)
    }
    
    func reIndex() {
        if advancedTableView == nil { return }
        for (i, row) in rows.enumerated() {
            row.index = i
        }
    }
    
    func heightForFooter() -> CGFloat? {
        if let height = footerHeight {
            return height
        }
        if let view = footerView {
            return view.bounds.height
        }
        return nil
    }
    
    func heightForHeader() -> CGFloat? {
        
        if let height = headerHeight {
            return height
        }
        if let view = headerView {
            return view.bounds.height
        }
        return nil
    }
    
    func shouldHighlight(row: Int) -> Bool {
        let row = rows[row]
        return row.shouldHighlight
    }
    
    func editingActions(row: Int) -> [UITableViewRowAction] {
        let row = rows[row]
        return row.rowActions
    }
    
    public func onCellResize() {
        UIView.setAnimationsEnabled(false)
        advancedTableView?.beginUpdatesIfNeeded()
        advancedTableView?.endUpdatesIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
}
