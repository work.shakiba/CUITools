//
//  AdvancedTableViewController.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 7/15/1396 AP.
//

import UIKit

open class AdvancedTableViewController: UIViewController {
    
    public var advancedTableView: AdvancedTableView!
    open var advancedTableViewStyle: UITableViewStyle {
        return .plain
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        setupAdvancedTableView()
    }
    
    fileprivate func setupAdvancedTableView() {
        advancedTableView = AdvancedTableView(frame: self.view.bounds, style: advancedTableViewStyle)
        advancedTableView.delegate = self
        advancedTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(advancedTableView)
    }
    
}

extension AdvancedTableViewController: AdvancedTableViewDelegate {
    
    open func setupTableView(dataSource: DataSource) {
        
    }
    
}
