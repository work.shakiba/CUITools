//
//  LoadingView.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/29/1396 AP.
//

import UIKit

public class LoadingView: UIView {
    
    let loadingViewContent: LoadingViewContent
    
    public required init(type: LoadingViewType, color: LoadingViewColorType, message: String? = nil) {
        loadingViewContent = LoadingViewContent(type: type, color: color, message: message)
        super.init(frame: .zero)
        addSubview(loadingViewContent)
        loadingViewContent.translatesAutoresizingMaskIntoConstraints = false
        loadingViewContent.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        loadingViewContent.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }
    
    public override init(frame: CGRect) {
        fatalError("LoadingView must be instantiated via the designated constructor")
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("LoadingView must be instantiated via the designated constructor")
    }
    
    public override func sizeThatFits(_ size: CGSize) -> CGSize {
        self.frame.size = self.intrinsicContentSize
        return self.intrinsicContentSize
    }
    
    override public var intrinsicContentSize: CGSize {
        return loadingViewContent.intrinsicContentSize
    }
    
}
