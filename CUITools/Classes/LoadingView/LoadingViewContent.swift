//
//  LoadingView.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/28/1396 AP.
//

import UIKit

public enum LoadingViewColorType {
    case accent(UIColor)
    case `default`
    case white
    
    var color: UIColor {
        switch self {
        case .accent(let color): return color
        case .default: return UIColor(hex: "BBBBBB")
        case .white: return UIColor.white
        }
    }
    
}

public enum LoadingViewType {
    case small
    case normal
    
    var size: CGSize {
        switch self {
        case .small: return CGSize(width: 25, height: 25)
        case .normal: return CGSize(width: 40, height: 40)
        }
    }
    
    var borderWith: CGFloat {
        switch self {
        case .small: return 2
        case .normal: return 3
        }
    }
    
}

public class LoadingViewContent: UIView {
    
    public let type: LoadingViewType
    public let colorType: LoadingViewColorType
    public private(set) var message: String?
    public let animationDuration: TimeInterval = 1
    public let animationStrokeEnd: CGFloat = 0.85
    fileprivate var shapeLayer: CAShapeLayer?

    public required init(type: LoadingViewType, color: LoadingViewColorType, message: String? = nil) {
        self.type = type
        self.colorType = color
        self.message = message
        let frame = CGRect.zero
        super.init(frame: frame)
        setupLayerView()
        setupMessageLabel()
    }
    
    public override init(frame: CGRect) {
        fatalError("LoadingView must be instantiated via the designated constructor")
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("LoadingView must be instantiated via the designated constructor")
    }
    
    public override var intrinsicContentSize: CGSize {
        return LoadingViewContent.size(type: type, message: message)
    }
    
    fileprivate func setupLayerView() {
        shapeLayer = layerForLoading()
        layer.addSublayer(shapeLayer!)
        animateRotation()
    }
    
    fileprivate func setupMessageLabel() {
        guard let message = self.message else { return }
        let label = UILabel()
        label.text = message
        label.font = Styles.global.fontNormal
        label.textColor = Styles.global.lightTextColor
        label.textAlignment = .center
        addSubview(label)
        let messageSize = message.sizeInView()
        let loadingSize = type.size
        label.frame = CGRect(x: 0, y: loadingSize.height, width: messageSize.width, height: messageSize.height)
    }
    
    fileprivate func layerForLoading() -> CAShapeLayer {
        let caShapeLayer = CAShapeLayer()
        let rect = CGRect(origin: .zero, size: self.intrinsicContentSize)
        let path = UIBezierPath(ovalIn: rect)
        caShapeLayer.path = path.cgPath
        caShapeLayer.strokeEnd = self.animationStrokeEnd
        caShapeLayer.lineCap = "round"
        caShapeLayer.lineWidth = type.borderWith
        caShapeLayer.strokeColor = colorType.color.cgColor
        caShapeLayer.fillColor = UIColor.clear.cgColor
        caShapeLayer.frame = CGRect(origin: .zero, size: type.size)
        return caShapeLayer
    }
    
//    fileprivate func animateStrokeEnd(_ layer: CAShapeLayer) {
//        layer.strokeStart = 0
//        CATransaction.begin()
//        CATransaction.setCompletionBlock {
//            CATransaction.setDisableActions(true)
//            layer.strokeEnd = self.animationStrokeEnd
//            self.animateRotation(layer)
//        }
//        let animation = CABasicAnimation(keyPath: "strokeEnd")
//        animation.fromValue = 0
//        animation.toValue = animationStrokeEnd
//        animation.duration = animationDuration
//        layer.add(animation, forKey: "StrokeAnimation")
//        CATransaction.commit()
//    }
    
    fileprivate func animateRotation() {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.fromValue = 0
        animation.toValue = CGFloat(CGFloat.pi * 2.0)
        animation.repeatCount = .infinity
        animation.duration = animationDuration
        animation.isRemovedOnCompletion = false
        shapeLayer!.add(animation, forKey: "RotateAnimation")
    }
    
    static func size(type: LoadingViewType, message: String?) -> CGSize {
        if let message = message {
            let loadingLayerSize = type.size
            let messageSize = message.sizeInView()
            let width = max(loadingLayerSize.width, messageSize.width)
            let height = loadingLayerSize.height + 16 + messageSize.height
            return CGSize(width: width, height: height)
        }else{
            return type.size
        }
    }
    
}
