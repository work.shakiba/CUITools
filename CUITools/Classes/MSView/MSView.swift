//
//  MSView.swift
//  Zaer
//
//  Created by mohsen shakiba on 7/20/1396 AP.
//  Copyright © 1396 mohsen shakiba. All rights reserved.
//

import UIKit

public class MSView: UIView {
    
    public weak var delegate: MSViewDelegate? {
        didSet {
            onDelegateSet()
        }
    }
    public var animationDuration: TimeInterval = 0.25
    public fileprivate(set) var currentView: UIView
    
    public init(frame: CGRect, initialView: UIView) {
        self.currentView = initialView
        super.init(frame: frame)
        setupInitialView()
    }
    
    override init(frame: CGRect) {
        fatalError()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    fileprivate func setupInitialView() {
        addSubview(currentView)
    }
    
    fileprivate func onDelegateSet() {
        self.delegate?.msView(viewAddedToSuperView: currentView, superView: self)
    }
    
    open override func updateConstraints() {
        currentView.frame = self.frame
        super.updateConstraints()
    }
    
}

extension MSView: MSViewProtocol {
    
    public func transit(to view: UIView) {
        let currentView = self.currentView
        if currentView == view {
            return
        }
        view.isHidden = true
        self.addSubview(view)
        self.delegate?.msView(viewAddedToSuperView: view, superView: self)
        self.delegate?.msView(transitionBegan: currentView, to: view)
        UIView.transition(from: currentView, to: view, duration: self.animationDuration, options: [.curveLinear, .transitionCrossDissolve,. showHideTransitionViews]) { (success) in
            self.delegate?.msView(transitionEnd: currentView, to: view, success: success)
            self.currentView = view
        }
    }
    
}

