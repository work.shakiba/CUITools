//
//  MSViewProtocol.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/27/1396 AP.
//

protocol MSViewProtocol {
    
    var currentView: UIView { get }
    var delegate: MSViewDelegate? { get set }
    
    func transit(to view: UIView)

}
