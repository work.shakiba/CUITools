//
//  MSViewDelegate.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/27/1396 AP.
//

import Foundation

public protocol MSViewDelegate: class {
    
    func msView(viewAddedToSuperView view: UIView, superView: UIView)
    func msView(transitionBegan from: UIView, to: UIView)
    func msView(transitionEnd from: UIView, to: UIView, success: Bool)

}

extension MSViewDelegate {
    
    public func msView(viewAddedToSuperView view: UIView, superView: UIView){}
    public func msView(transitionBegan from: UIView, to: UIView){}
    public func msView(transitionEnd from: UIView, to: UIView, success: Bool){}
    
}
