//
//  MSViewController.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/28/1396 AP.
//

import UIKit


open class MSViewController: UIViewController {
    
    open var initialView: UIView {
        fatalError("initial view not provided")
    }
    
    fileprivate var _msView: MSView!
    public var msView: MSView {
        if _msView == nil {
            fatalError("msView is aviailable after viewDidLoad")
        }
        return _msView
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupMSView()
    }
    
    fileprivate func setupMSView() {
        _msView = MSView(frame: self.view.frame, initialView: self.initialView)
        view.addSubview(_msView)
        _msView.translatesAutoresizingMaskIntoConstraints = false
        _msView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        _msView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        _msView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        _msView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
    }
    
    public func transit(to view: UIView) {
        _msView.transit(to: view)
    }
    
    
}
