//
//  MSTableViewController.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/29/1396 AP.
//

import UIKit

public class MSTableView: UIView {
    
    public private(set) var msView: MSView!
    public private(set) var tableView: UITableView!
    public private(set) var tableViewStyle: UITableViewStyle!

    public init(frame: CGRect, tableViewStyle: UITableViewStyle) {
        self.tableViewStyle = tableViewStyle
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupMSViewIfNeeded(with initialView: UIView) {
        if msView != nil { return }
        msView = MSView(frame: self.bounds, initialView: initialView)
        addSubview(msView)
        msView.translatesAutoresizingMaskIntoConstraints = false
        msView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        msView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        msView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        msView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        msView.delegate = self
    }
    
    fileprivate func setupTableView() {
        if tableView == nil {
            tableView = UITableView(frame: .zero, style: tableViewStyle)
        }
    }
    
    public func showLoading(message: String? = nil) {
        let loadingView = LoadingView(type: .normal, color: .default, message: message)
        transit(to: loadingView)
    }
    
    public func showTableView() {
        transit(to: tableView)
    }
    
    public func showError(message: String? = nil, image: UIImage? = nil) {
        let errorView = ErrorView(message: message, image: image)
        transit(to: errorView)
    }
    
    fileprivate func transit(to view: UIView) {
        if msView == nil {
            setupMSViewIfNeeded(with: view)
        }else{
            msView.transit(to: view)
        }
    }
    
}

extension MSTableView: MSViewDelegate {
    
    public func msView(viewAddedToSuperView view: UIView, superView: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: superView.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive = true
        view.leftAnchor.constraint(equalTo: superView.leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: superView.rightAnchor).isActive = true
    }
    
}
