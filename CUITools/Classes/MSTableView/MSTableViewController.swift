//
//  MSTableViewController.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/29/1396 AP.
//

import UIKit

open class MSTableViewController: UIViewController {
    
    public var msTableView: MSTableView!
    public var refreshContrl = UIRefreshControl()
    
    open var shouldEnablePullToRefresh: Bool {
        return false
    }
    
    public var tableView: UITableView {
        return msTableView.tableView
    }
    
    open var tableViewStyle: UITableViewStyle {
        return .plain
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupMSTableView()
        setupRefreshControlView()
    }
    
    fileprivate func setupRefreshControlView() {
        if !shouldEnablePullToRefresh {
            if #available(iOS 10.0, *) {
                tableView.refreshControl = nil
            } else {
                // Fallback on earlier versions
            }
            return
        }
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshContrl
        } else {
            // Fallback on earlier versions
        }
        refreshContrl.addTarget(self, action: #selector(refreshContrlValueChanged), for: .valueChanged)
    }
    
    fileprivate func setupMSTableView() {
        msTableView = MSTableView(frame: .zero, tableViewStyle: tableViewStyle)
        view.addSubview(msTableView)
        msTableView.translatesAutoresizingMaskIntoConstraints = false
        msTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        msTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        msTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        msTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    public func showLoading(message: String? = nil) {
        msTableView.showLoading(message: message)
    }
    
    public func showTableView() {
        msTableView.showTableView()
    }
    
    public func showError(message: String? = nil, image: UIImage? = nil) {
        msTableView.showError(message: message, image: image)
    }
    
    @objc func refreshContrlValueChanged() {
        refreshDidBegin()
    }
    
    open func refreshDidBegin() {
        refreshContrl.beginRefreshing()
    }
    
    public func endRefresh() {
        refreshContrl.endRefreshing()
    }
    
}


