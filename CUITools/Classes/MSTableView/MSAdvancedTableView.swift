//
//  MSTableViewController.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/29/1396 AP.
//

import UIKit
import AdvancedTableView

open class MSAdvancedTableViewController: UIViewController {
    
    public var msTableView: MSAdvancedTableView!
    public var refreshContrl = UIRefreshControl()
    
    open var advancedTableViewStyle: UITableViewStyle {
        return .plain
    }
    
    open var shouldEnablePullToRefresh: Bool {
        return false
    }
    
    public var advancedTableView: AdvancedTableView {
        return msTableView.advancedTableView
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupMSTableView()
        setupRefreshControlView()
    }
    
    fileprivate func setupRefreshControlView() {
        if !shouldEnablePullToRefresh {
            if #available(iOS 10.0, *) {
                advancedTableView.tableView.refreshControl = nil
            } else {
                // Fallback on earlier versions
            }
            return
        }
        if #available(iOS 10.0, *) {
            advancedTableView.tableView.refreshControl = refreshContrl
        } else {
            // Fallback on earlier versions
        }
        refreshContrl.addTarget(self, action: #selector(refreshContrlValueChanged), for: .valueChanged)
    }
    
    fileprivate func setupMSTableView() {
        msTableView = MSAdvancedTableView(frame: .zero, style: advancedTableViewStyle)
        msTableView.advancedTableView.delegate = self
        view.addSubview(msTableView)
        msTableView.translatesAutoresizingMaskIntoConstraints = false
        msTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        msTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        msTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        msTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    public func showLoading(message: String? = nil) {
        msTableView.showLoading(message: message)
    }
    
    public func showTableView() {
        msTableView.showTableView()
    }
    
    public func showError(message: String? = nil, image: UIImage? = nil) {
        msTableView.showError(message: message, image: image)
    }
    
    @objc func refreshContrlValueChanged() {
        refreshDidBegin()
    }
    
    open func refreshDidBegin() {
        refreshContrl.beginRefreshing()
    }
    
    public func endRefresh() {
        refreshContrl.endRefreshing()
    }
    
}


extension MSAdvancedTableViewController: AdvancedTableViewDelegate {
    
    open func setupTableView(dataSource: DataSource) {
        
    }
    
}

open class MSAdvancedTableView: UIView {
    
    public private(set) var msView: MSView!
    public private(set) var advancedTableView: AdvancedTableView!
    
    public init(frame: CGRect, style: UITableViewStyle) {
        advancedTableView = AdvancedTableView(frame: .zero, style: style)
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupMSViewIfNeeded(with initialView: UIView) {
        if msView != nil { return }
        msView = MSView(frame: self.bounds, initialView: initialView)
        addSubview(msView)
        msView.translatesAutoresizingMaskIntoConstraints = false
        msView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        msView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        msView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        msView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        msView.delegate = self
    }
    
    public func showLoading(message: String? = nil) {
        let loadingView = LoadingView(type: .normal, color: .default, message: message)
        transit(to: loadingView)
    }
    
    public func showTableView() {
        transit(to: advancedTableView)
    }
    
    public func showError(message: String? = nil, image: UIImage? = nil) {
        let errorView = ErrorView(message: message, image: image)
        transit(to: errorView)
    }
    
    fileprivate func transit(to view: UIView) {
        if msView == nil {
            setupMSViewIfNeeded(with: view)
        }else{
            msView.transit(to: view)
        }
    }
    
}

extension MSAdvancedTableView: MSViewDelegate {
    
    open func msView(viewAddedToSuperView view: UIView, superView: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: superView.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive = true
        view.leftAnchor.constraint(equalTo: superView.leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: superView.rightAnchor).isActive = true
    }
    
}

