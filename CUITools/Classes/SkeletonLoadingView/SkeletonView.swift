//
//  SkeletonView.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/3/1396 AP.
//

import UIKit

public enum SkeletonViewItemType {
    case text
    case image
    
    var size: (width: Int, height: Int) {
        switch self {
        case .image: return (3, 3)
        case .text: return (0, 1)
        }
    }
    
}

public enum SkeletonViewDirection {
    case right
    case left
}

public struct SkeletonViewItem {
    
    let x: Int
    let y: Int
    let type: SkeletonViewItemType
    
    let padding: CGFloat = 16
    
    init(x: Int, y: Int, type: SkeletonViewItemType) {
        self.x = x
        self.y = y
        self.type = type
    }
    
    func drawIn(superLayer: CALayer) {
        let layer = CALayer()
        layer.backgroundColor = UIColor.gray.cgColor
        let totalWidth = UIScreen.main.bounds.width - (CGFloat(2 + x) * padding)
        let requiredWidth = CGFloat(type.size.width) * padding
        let width = type.size.width == 0 ? totalWidth : requiredWidth
        let height = CGFloat(type.size.height) * padding
        let frame = CGRect(x: 0, y: (CGFloat(y) * padding), width: width, height: height)
        layer.cornerRadius = height / 2
        layer.frame = frame
        superLayer.addSublayer(layer)
    }
    
}

public class SkeletonViewDefinition {
    
    fileprivate var currentLine: Int = 1
    fileprivate var subviews: [SkeletonViewItem] = []
    
    public init() {
        
    }
    
    public func addSubview(offset: Int, type: SkeletonViewItemType, direction: SkeletonViewDirection) {
        let item = SkeletonViewItem(x: offset, y: currentLine, type: type)
        self.subviews.append(item)
    }
    
    public func goToNextLine() {
        currentLine += 2
    }
    
}

public class SkeletonView: UIView {
    
    let definition: SkeletonViewDefinition
    
    public init(frame: CGRect, definition: SkeletonViewDefinition) {
        self.definition = definition
        super.init(frame: frame)
        setupLoadingView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    fileprivate func setupLoadingView() {
        for item in definition.subviews {
            item.drawIn(superLayer: self.layer)
        }
    }
    
}
