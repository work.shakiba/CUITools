//
//  LoadingImageView.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/30/1396 AP.
//

import UIKit
import SwiftHTTP

public extension UIImageView {
    
    public var cornerToBounds: Bool {
        set {
            self.layer.cornerRadius = bounds.width / 2
        }
        get {
            return false
        }
    }
    
    public var url: String? {
        set {
            onUrlSet(newValue)
            return self.layer.setValue(newValue, forKey: "url")
        }
        get {
            return self.layer.value(forKey: "url") as? String
        }
    }
    
    public var defaultImage: UIImage? {
        set {
            self.image = newValue
            return self.layer.setValue(newValue, forKey: "defaultImage")
        }
        get {
            return self.layer.value(forKey: "defaultImage") as? UIImage
        }
    }
    
    fileprivate var currentRequest: HTTP? {
        set {
            return self.layer.setValue(newValue, forKey: "httpRequest")
        }
        get {
            return self.layer.value(forKey: "httpRequest") as? HTTP
        }
    }
    
    func onUrlSet(_ url: String?) {
        currentRequest?.cancel()
        self.image = defaultImage
        guard let url = url else {
            return
        }
        if let url = URL(string: url) {
            let req = URLRequest(url: url)
            let task = HTTP(req)
            currentRequest = task
            task.run { (response) in
                if response.error != nil {
                    return
                }
                self.onImageDownloaded(response.data)
            }
        }
        
        
    }
    
    func onDefaultImageSet() {
        self.image = defaultImage
    }
    
    func onImageDownloaded(_ data: Data) {
        if data.isEmpty {
            return
        }
        guard let image = UIImage(data: data) else {
            return
        }
        DispatchQueue.main.async {
            let currentSize = self.bounds.size
            DispatchQueue.global(qos: .userInteractive).async {
                self.onImageReady(size: currentSize, image: image)
            }
        }
        
    }
    
    fileprivate func onImageReady(size: CGSize, image: UIImage) {
        let maxSize = max(image.size.width, image.size.height)
        let cropSize = CGSize(width: maxSize, height: maxSize)
        let croppedImage = crop(image: image, to: cropSize, size: size)

        DispatchQueue.main.async {
            UIView.transition(with: self, duration: 0.15, options: [.transitionCrossDissolve, .curveLinear], animations: {
                self.image = croppedImage
            }, completion: nil)
        }
    }
    
    func crop(image: UIImage, to:CGSize, size: CGSize) -> UIImage {
        guard let cgimage = image.cgImage else { fatalError() }
        
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        
        let contextSize: CGSize = contextImage.size
        
        let scale = UIScreen.main.scale
        
        //Set to square
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        let cropAspect: CGFloat = to.width / to.height
        
        var cropWidth: CGFloat = to.width
        var cropHeight: CGFloat = to.height
        
        if to.width > to.height { //Landscape
            cropWidth = contextSize.width
            cropHeight = contextSize.width / cropAspect
            posY = (contextSize.height - cropHeight) / 2
        } else if to.width < to.height { //Portrait
            cropHeight = contextSize.height
            cropWidth = contextSize.height * cropAspect
            posX = (contextSize.width - cropWidth) / 2
        } else { //Square
            if contextSize.width >= contextSize.height { //Square on landscape (or square)
                cropHeight = contextSize.height
                cropWidth = contextSize.height * cropAspect
                posX = (contextSize.width - cropWidth) / 2
            }else{ //Square on portrait
                cropWidth = contextSize.width
                cropHeight = contextSize.width / cropAspect
                posY = (contextSize.height - cropHeight) / 2
            }
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let cropped: UIImage = UIImage(cgImage: imageRef, scale: scale, orientation: image.imageOrientation)
        
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        cropped.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resized = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resized!
    }
    
}
