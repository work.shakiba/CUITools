//
//  StateView.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/27/1396 AP.
//

import UIKit

public class ErrorViewContent: UIView {
    
    public private(set) var message: String?
    public private(set) var image: UIImage?
    public private(set) var action: String?
    
    public private(set) var label: UILabel?
    public private(set) var imageView: UIImageView?
    public private(set) var button: UIButton?
    
    public init(message: String?, image: UIImage?, action: String?) {
        self.message = message
        self.image = image
        self.action = action
        super.init(frame: .zero)
        setupMessageIfNeeded()
        setupImageViewIfNeeded()
    }
    
    override init(frame: CGRect) {
        fatalError("ErroView must be initialized view the designated constructor")
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("ErroView must be initialized view the designated constructor")
    }
    
    fileprivate func setupMessageIfNeeded() {
        guard let message = message else { return }
        label = UILabel()
        label?.numberOfLines = 99
        label?.text = message
        label?.font = Styles.global.fontNormal
        label?.textColor = Styles.global.lightTextColor
        addSubview(label!)
        
    }
    
    fileprivate func setupImageViewIfNeeded() {
        guard let image = image else { return }
        imageView = UIImageView()
        imageView?.image = image
        
        addSubview(imageView!)
    }
    
    
    override public var intrinsicContentSize: CGSize {
        var totalSize = CGSize.zero
        if let message = message {
            let messageSize = message.sizeInView(horizontalPadding: 24, font: Styles.global.fontNormal)
            totalSize.height += messageSize.height + 24
            totalSize.width = max(messageSize.width, totalSize.width)
        }
        if let image = image {
            totalSize.height += image.size.height
            totalSize.width = max(image.size.width, totalSize.width)
        }
        if let action = action {
            let actionSize = action.sizeInView(horizontalPadding: 24, font: Styles.global.fontNormal)
            totalSize.height += actionSize.height + 24
            totalSize.width = max(actionSize.width, totalSize.width)
        }
        return totalSize
    }
    
    public override func layoutSubviews() {
        let totalSize = bounds
        if let message = message {
            let labelSize = message.sizeInView(horizontalPadding: 24, font: Styles.global.fontNormal)
            if let image = image {
                label?.frame = CGRect(x: 0, y: image.size.height + 24, width: totalSize.width, height: labelSize.height)
            }else{
                label?.frame = CGRect(x: 0, y: 0, width: totalSize.width, height: labelSize.height)
            }
        }
        if let image = image {
            let totalSize = intrinsicContentSize
            let imagePosX = (totalSize.width - image.size.width) / 2
            imageView?.frame = CGRect(x: imagePosX, y: 0, width: image.size.width, height: image.size.height)
        }
        
        super.layoutSubviews()
    }

    
}

