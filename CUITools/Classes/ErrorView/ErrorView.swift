//
//  ErrorView.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/10/1396 AP.
//

import UIKit


public class ErrorView: UIView {
    
    var errorViewContent: ErrorViewContent
    
    public init(message: String?, image: UIImage?) {
        errorViewContent = ErrorViewContent(message: message, image: image, action: nil)
        super.init(frame: .zero)
        addSubview(errorViewContent)
        errorViewContent.translatesAutoresizingMaskIntoConstraints = false
        errorViewContent.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        errorViewContent.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    public override var intrinsicContentSize: CGSize {
        return errorViewContent.intrinsicContentSize
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override init(frame: CGRect) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
