//
//  IndicatorViewProtocol.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/27/1396 AP.
//

import Foundation

public protocol ErrorViewProtocol {
    
    func showState(message: String)
    func showState(message: String, image: UIImage)
    func showState(title: String, message: String, image: UIImage)
    
}
