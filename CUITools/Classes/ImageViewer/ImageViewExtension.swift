//
//  ImageViewExtension.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/6/1396 AP.
//

import UIKit
import ImageViewer

public protocol ImageViewerExtension {
    
}

extension ImageViewerExtension where Self: UIViewController {
    
    public func presentImage(image: UIImage) {
        let ds = ImageViewerDataSource(images: [image])
        self.view.layer.setValue(ds, forKey: "image_data_source")
        let vc = GalleryViewController(startIndex: 0, itemsDataSource: ds, itemsDelegate: nil, displacedViewsDataSource: nil, configuration: [GalleryConfigurationItem.deleteButtonMode(.none), GalleryConfigurationItem.thumbnailsButtonMode(.none)])
        self.presentImageGallery(vc)
    }
    
}

class ImageViewerDataSource: GalleryItemsDataSource {
    
    var images: [UIImage] = []

    init(images: [UIImage]) {
        self.images = images
    }
    
    func itemCount() -> Int {
        return images.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        let item = GalleryItem.image { (handler) in
            return handler(self.images[index])
        }
        return item
    }
    
}
