//
//  DefaultStyles.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/28/1396 AP.
//

import UIKit

public class Styles {
    
    public static var global = Styles()
    
    fileprivate var areFontsInstalled = false
    
    public var fontNormalSize: CGFloat {
        return fontSizeBase + 0
    }
    public var fontLargeSize: CGFloat {
        return fontSizeBase + 2
    }
    public var fontSmallSize: CGFloat {
        return fontSizeBase - 2
    }
    public var fontTinySize: CGFloat {
        return fontSizeBase - 4
    }

    public var fontSizeBase: CGFloat = 16
    
    public var fontNormalName: String = "IRANSansMobile"
    public var fontThinName: String = "IRANSansMobile"
    public var fontBoldName: String = "IRANSansMobile"
    
    public var fontNormalFileName: String = "iranSans"
    public var fontThinFileName: String = "iranSans"
    public var fontBoldFileName: String = "iranSans"
    
    public var _accentColor: UIColor = UIColor(hex: "ff4d4d")
    public var _textColor: UIColor = UIColor(hex: "555555")
    public var _lightTextColor: UIColor = UIColor(hex: "888888")
    public var _extraLightTextColor: UIColor = UIColor(hex: "AAAAAA")
    public var _lightBackgroundColor: UIColor = UIColor(hex: "f5f5f5")
    public var _borderColor: UIColor = UIColor(hex: "EEEEEE")
    public var _borderColorDark: UIColor = UIColor(hex: "CCCCCC")

    public func font(size: CGFloat, name: String) -> UIFont {
        setupFontsIfNeeded()
        return UIFont(name: name, size: size)!
    }
    
    public var fontNormal: UIFont {
        return font(size: fontNormalSize, name: fontNormalName)
    }
    
    public var fontLarge: UIFont {
        return font(size: fontLargeSize, name: fontNormalName)
    }
    
    public var fontSmall: UIFont {
        return font(size: fontSmallSize, name: fontNormalName)
    }
    
    public var fontTiny: UIFont {
        return font(size: fontTinySize, name: fontNormalName)
    }
    
    public var accentColor: UIColor {
        return _accentColor
    }
    
    public var textColor: UIColor {
        return _textColor
    }
    
    public var lightTextColor: UIColor {
        return _lightTextColor
    }
    
    public var extraLightTextColor: UIColor {
        return _extraLightTextColor
    }
    
    public var lightBackgroundColor: UIColor {
        return _lightBackgroundColor
    }
    
    public var borderColor: UIColor {
        return _borderColor
    }
    
    public var darkBorderColor: UIColor {
        return _borderColorDark
    }
    
    
    func installFont(_ font: String) {
        
        guard let fontUrl = Bundle.assetsBundle.url(forResource: font, withExtension: "ttf") else {
            return
        }
        
        let fontData = try! Data(contentsOf: fontUrl)
        
        if let provider = CGDataProvider.init(data: fontData as CFData) {
            
            var error: Unmanaged<CFError>?
            
            let font:CGFont = CGFont(provider)
            if (!CTFontManagerRegisterGraphicsFont(font, &error)) {
                print(error.debugDescription)
                return
            } else {
                return
            }
        }
        return
    }
    
    func setupFontsIfNeeded() {
        if areFontsInstalled {
            return
        }
        areFontsInstalled = true
        installFont(fontNormalFileName)
//        installFont(fontThinFileName)
//        installFont(fontBoldFileName)
    }
    
    public var _defaultBorderRadius: CGFloat = 20
    
    public var defaultBorderRadius: CGFloat {
        return _defaultBorderRadius
    }

    public var _defaultDisabledButtonColor: UIColor = UIColor(hex: "BBBBBB")
    
    public var defaultDisabledButtonColor: UIColor {
        return _defaultDisabledButtonColor
    }
    
    public var _defaultTextAlignment: NSTextAlignment = .right
    
    public var textAlignment: NSTextAlignment {
        return _defaultTextAlignment
    }
    
    public var _errorColor: UIColor = UIColor(hex: "ff4d4d")
    
    public var errorColor: UIColor {
        return _errorColor
    }
    
}
