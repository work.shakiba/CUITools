//
//  FormViewController.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/3/1396 AP.
//

import UIKit

open class FormView: UIView {
    
    public let stackView = UIStackView()
    public let scrollView = UIScrollView()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupSubviews() {
        addSubview(scrollView)
        scrollView.alwaysBounceVertical = true
        scrollView.addSubview(stackView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.scrollView.leftAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.scrollView.rightAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor).isActive = true
    }
    
    public func add(form: FormViewProtocol, paddingTop: CGFloat = 0, paddingBottom: CGFloat = 0) {
        addToStackView(form: form, at: stackView.arrangedSubviews.count, paddingTop: paddingTop, paddingBottom: paddingBottom)
    }
    
    public func insert(form: FormViewProtocol, at: Int, paddingTop: CGFloat = 0, paddingBottom: CGFloat = 0) {
        addToStackView(form: form, at: at, paddingTop: paddingTop, paddingBottom: paddingBottom)
    }
    
    public func remove(accessory: UIView) {
        stackView.removeArrangedSubview(accessory)
    }
    
    public func remove(form: FormViewProtocol) {
        stackView.removeArrangedSubview(form as! UIView)
    }
    
    func addToStackView(form: FormViewProtocol, at: Int, paddingTop: CGFloat, paddingBottom: CGFloat) {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onFormItemTapped(_:)))
        let containerView = FormViewContainer(form: form, topPadding: paddingTop, bottomPadding: paddingBottom)
        containerView.addGestureRecognizer(tapGestureRecognizer)
        stackView.insertArrangedSubview(containerView, at: at)
    }
    
    public func insert(accessory: UIView, at: Int) {
        stackView.insertArrangedSubview(accessory, at: at)
    }
    
    public func add(accessory: UIView) {
        stackView.addArrangedSubview(accessory)
    }
    
    public func scrollToFirstItemWithError () {
    }
    
    @objc func onFormItemTapped(_ tapGestureRecognizer: UITapGestureRecognizer) {
        guard let formContainer = tapGestureRecognizer.view else {
            return
        }
        stackView.arrangedSubviews.forEach { (view) in
            guard let container = view as? FormViewContainer else {
                return
            }
            if container == formContainer {
                container.selected = true
            }else{
                container.selected = false
            }
        }
    }
    
}

