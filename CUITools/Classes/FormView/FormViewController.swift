//
//  FormViewController.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/3/1396 AP.
//

import UIKit

open class FormViewController: UIViewController {
    
    public let formView = FormView(frame: .zero)

    open override func viewDidLoad() {
        super.viewDidLoad()
        setupStackView()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(_:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(_:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        formView.scrollView.keyboardDismissMode = .interactive
    }
    
    fileprivate func setupStackView() {
        view.addSubview(formView)
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        formView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        formView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        formView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }

    @objc func keyboardWillAppear(_ notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let inset = keyboardFrame.height // if scrollView is not aligned to bottom of screen, subtract offset
            formView.scrollView.contentInset.bottom = inset
            formView.scrollView.scrollIndicatorInsets.bottom = inset
            formView.stackView.arrangedSubviews.forEach { (view) in
                guard let container = view as? FormViewContainer else {
                    return
                }
                if container.selected {
                    formView.scrollView.scrollRectToVisible(container.frame , animated: true)
                }
            }
        }
    }
    
    @objc func keyboardWillDisappear(_ notification: NSNotification) {
        formView.scrollView.contentInset.bottom = 0
        formView.scrollView.scrollIndicatorInsets.bottom = 0
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
