//
//  ValidationProtocol.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/4/1396 AP.
//

import Foundation

public protocol ValidationProtocol {
    
    func isValid(title: String, value: String?) -> String?
    
}

public class StringNotNullValidator: ValidationProtocol {
    
    public func isValid(title: String, value: String?) -> String? {
        if value == nil || value?.isEmpty == true {
            return "فبلد \(title) پر نشده"
        }
        return nil
    }
    
}

public class StringLengthValidator: ValidationProtocol {
    
    let minLength: Int
    let maxLength: Int
    let isNullable: Bool

    public init(nullable: Bool, minLength: Int, maxLength: Int) {
        self.isNullable = nullable
        self.minLength = minLength
        self.maxLength = maxLength
    }
    
    public func isValid(title: String, value: String?) -> String? {
        guard let value = value else {
            if isNullable {
                return nil
            }else{
                return "فیلد \(title) پر نشده"
            }
        }
        if value.characters.count < minLength {
            return "فیلد \(title) باید حداقل \(minLength) کاراکتر باشد"
        }
        if value.characters.count > maxLength {
            return "فیلد \(title) باید حذاکثر \(maxLength) کاراکتر باشد"
        }
        return nil
    }
    
}
