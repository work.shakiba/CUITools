//
//  FormViewProtocol.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/9/1396 AP.
//

import UIKit

class FormViewContainer: UIView {
    
    let form: FormViewProtocol
    var topPadding: CGFloat
    var bottomPadding: CGFloat
    var selected: Bool = false {
        didSet {
            onSelectionChanged()
        }
    }
    
    init(form: FormViewProtocol, topPadding: CGFloat, bottomPadding: CGFloat) {
        self.form = form
        self.topPadding = topPadding
        self.bottomPadding = bottomPadding
        super.init(frame: .zero)
        setupFormView()
    }
    
    fileprivate func setupFormView() {
        let formView = form as! UIView
        addSubview(formView)
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.topAnchor.constraint(equalTo: self.topAnchor, constant: topPadding).isActive = true
        formView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -bottomPadding).isActive = true
        formView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        formView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        guard let formView = form as? UIView else {
            fatalError("form must be subclass of UIView")
        }
        return CGSize(width: UIScreen.main.bounds.width, height: formView.intrinsicContentSize.height + topPadding + bottomPadding)
    }
    
    fileprivate func onSelectionChanged() {
        if selected {
            form.selected()
            (form as! UIControl).sendActions(for: .touchUpInside)
        }else{
            form.deselected()
        }
    }
    
}
