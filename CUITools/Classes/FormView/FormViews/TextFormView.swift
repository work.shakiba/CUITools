//
//  FormItemView.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/3/1396 AP.
//

import UIKit
import SnapKit

public class TextFormView: UIControl {
    
    // MARK: properties
    
    public let title: String
    public var value: String?
    public var error: String?
    
    fileprivate let titleTopPadding: CGFloat = 20
    fileprivate let titleHeight: CGFloat = 20
    fileprivate let borderHeight: CGFloat = 2
    fileprivate let sidePadding: CGFloat = 16
    fileprivate let errorHeight: CGFloat = 20
    fileprivate let bottomPadding: CGFloat = 8
    fileprivate let elementPadding: CGFloat = 5
    
    fileprivate var textViewSidePadding: CGFloat {
        return sidePadding - 4
    }
    
    fileprivate var textViewHeight: CGFloat {
        let value = textView.text.isEmpty ? "test" : textView.text!
        let textViweSize = value.sizeInView(horizontalPadding: textView.frame.origin.x, font: textView.font!)
        return textViweSize.height
    }
    
    override public var intrinsicContentSize: CGSize {
        var size = textViewHeight + titleTopPadding + bottomPadding + (2 * elementPadding)
        if self.error != nil {
            size += errorHeight
        }
        return CGSize(width: UIScreen.main.bounds.width, height: size)
    }
    
    // MARK: subviews
    
    public let textView = UITextView()
    public var borderView = UIView()
    public let titleLabel = UILabel()
    public let errorView = UILabel()
    
    // MARK: initializers
    
    public init(title: String, value: String?) {
        self.title = title
        self.value = value
        super.init(frame: .zero)
        setupTitleLable()
        setupTextView()
        setupBorderView()
        setFrameForBorder()
        changeAccentColor()
        resetValues()
        changeTitleTransform()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: setup subviews
    
    fileprivate func setupTitleLable() {
        self.addSubview(titleLabel)
        titleLabel.font = Styles.global.fontNormal
        titleLabel.frame = CGRect(x: sidePadding, y: titleTopPadding, width: UIScreen.main.bounds.width - (2 * sidePadding), height: titleHeight)
        // can't use autolayout since the anchor point mess things up
        //        titleLabel.snp.makeConstraints { (make) in
        //            make.top.left.right.equalTo(self).inset(16)
        //            make.height.equalTo(25)
        //        }
        titleLabel.text = title
        titleLabel.textAlignment = Styles.global.textAlignment
        setAnchorPoint(CGPoint(x: 1, y: 0), forView: titleLabel)
    }
    
    
    fileprivate func setupTextView() {
        addSubview(textView)
        textView.isScrollEnabled = false
        textView.backgroundColor = .clear
        textView.textContainerInset = .zero
        textView.font = Styles.global.fontNormal
        textView.textColor = Styles.global.textColor
        textView.textAlignment = Styles.global.textAlignment
        textView.delegate = self
        textView.frame = CGRect(x: textViewSidePadding, y: titleTopPadding + elementPadding, width: UIScreen.main.bounds.width - (2 * textViewSidePadding), height: textViewHeight)
    }
    
    fileprivate func setupBorderView() {
        addSubview(borderView)
    }
    
    // MARK: change methods
    
    fileprivate func changeAccentColor() {
        let titleColor: UIColor
        let borderColor: UIColor
        if error != nil {
            titleColor = Styles.global.errorColor
            borderColor = Styles.global.errorColor
        }else{
            titleColor = textView.isFirstResponder ? Styles.global.accentColor : Styles.global.extraLightTextColor
            borderColor = textView.isFirstResponder ? Styles.global.accentColor : Styles.global.borderColor
        }
        titleLabel.textColor = titleColor
        borderView.backgroundColor = borderColor
    }
    
    fileprivate func changeTitleTransform() {
        let transform: CATransform3D
        let isInValueMode = textView.isFirstResponder || textView.text.isEmpty == false
        if isInValueMode {
            let transformScale = CATransform3DMakeScale(0.7, 0.7, 1)
            let transformTranslate = CATransform3DTranslate(CATransform3DIdentity, 0, -20, 0)
            transform = CATransform3DConcat(transformTranslate, transformScale)
        }else{
            transform = CATransform3DIdentity
        }
        self.titleLabel.layer.transform = transform
    }
    
    fileprivate func setFrameForBorder() {
        borderView.frame = CGRect(x: sidePadding, y: titleTopPadding + textViewHeight + (2 * elementPadding), width: UIScreen.main.bounds.width - (2 * sidePadding), height: 2)
    }
    
    fileprivate func setFrameForTextView() {
        textView.frame.size.height = textViewHeight
    }
    
    fileprivate func animateChanges() {
        UIView.animate(withDuration: 0.25, animations: {
            self.changeTitleTransform()
            self.changeAccentColor()
            self.setFrameForBorder()
        })
    }
    
    func setAnchorPoint(_ anchorPoint: CGPoint, forView view: UIView) {
        if view.layer.anchorPoint == anchorPoint { return }
        var newPoint = CGPoint(x: view.bounds.size.width * anchorPoint.x, y: view.bounds.size.height * anchorPoint.y)
        var oldPoint = CGPoint(x: view.bounds.size.width * view.layer.anchorPoint.x, y: view.bounds.size.height * view.layer.anchorPoint.y)
        
        newPoint = newPoint.applying(view.transform)
        oldPoint = oldPoint.applying(view.transform)
        
        var position = view.layer.position
        position.x -= oldPoint.x
        position.x += newPoint.x
        
        position.y -= oldPoint.y
        position.y += newPoint.y
        
        view.layer.position = position
        view.layer.anchorPoint = anchorPoint
    }
    
}

extension TextFormView: FormViewProtocol, FormViewValidatableProtocol {
    
    public func resetValues() {
        textView.text = value
    }
    
    public func getValue() -> String? {
        return self.textView.text
    }
    
    public func getTitle() -> String {
        return self.title
    }
    
    public func selected() {
        self.textView.isUserInteractionEnabled = true
        self.textView.becomeFirstResponder()
    }
    
    public func deselected() {
        self.textView.isUserInteractionEnabled = false
        self.textView.resignFirstResponder()
    }
    
    public func showError(error: String) {
        self.error = error
        addSubview(errorView)
        errorView.text = error
        errorView.textColor = Styles.global.errorColor
        errorView.textAlignment = Styles.global.textAlignment
        errorView.font = Styles.global.fontTiny
        errorView.frame = CGRect(x: sidePadding, y: titleTopPadding + textViewHeight + 2 + (2 * elementPadding), width: UIScreen.main.bounds.width - (2 * sidePadding), height: errorHeight)
        changeAccentColor()
        invalidateIntrinsicContentSize()
    }
    
    public func clearError() {
        self.error = nil
        errorView.removeFromSuperview()
        changeAccentColor()
        invalidateIntrinsicContentSize()
    }
    
}

extension TextFormView: UITextViewDelegate {
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        animateChanges()
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        value = textView.text
        invalidateIntrinsicContentSize()
        setFrameForBorder()
        setFrameForTextView()
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        animateChanges()
    }
    
}

