//
//  SelectFormView.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/5/1396 AP.
//

import UIKit

public class SelectFormView: UIControl {
    
    let titleLabel = UILabel()
    let detailLabel = UILabel()
    let indicatorImage = UIImageView()
    let errorLabel = UILabel()
    let borderView = UIView()
    
    let verticalPadding: CGFloat = 8
    let horizontalPadding: CGFloat = 16
    let labelHeight: CGFloat = 30
    let errorHeight: CGFloat = 20
    
    public var title: String
    public var value: String?
    var error: String?
    
    public init(title: String, value: String?) {
        self.title = title
        self.value = value
        super.init(frame: .zero)
        setupSubViews()
        changeAccentColor()
        setupBorderView()
        resetValues()
    }
    
    override public var intrinsicContentSize: CGSize {
        var height: CGFloat = labelHeight + (2 * verticalPadding)
        if error != nil {
            height += errorHeight
        }
        return CGSize(width: UIScreen.main.bounds.width, height: height)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupSubViews() {
        titleLabel.font = Styles.global.fontNormal
        detailLabel.font = Styles.global.fontNormal
        titleLabel.textAlignment = .right
        detailLabel.textAlignment = .left
        titleLabel.text = title
        detailLabel.text = value
        addSubview(titleLabel)
        addSubview(detailLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.right.equalTo(self).inset(horizontalPadding)
            make.top.equalTo(self).inset(verticalPadding)
            make.left.equalTo(detailLabel.snp.right)
            make.height.equalTo(self.labelHeight)
        }
        detailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self).inset(horizontalPadding)
            make.top.bottom.equalTo(self).inset(verticalPadding)
            make.height.equalTo(self.labelHeight)
        }
        detailLabel.setContentHuggingPriority(250, for: .horizontal)
    }
    
    fileprivate func changeAccentColor() {
        let color: UIColor
        let borderColor: UIColor
        if error != nil {
            color = Styles.global.errorColor
            borderColor = Styles.global.errorColor
        }else if isSelected{
            color = Styles.global.accentColor
            borderColor = Styles.global.accentColor
        }else{
            color = Styles.global.lightTextColor
            borderColor = Styles.global.borderColor
        }
        self.titleLabel.textColor = color
        self.detailLabel.textColor = Styles.global.textColor
        self.borderView.backgroundColor = borderColor
    }
    
    fileprivate func setupBorderView() {
        addSubview(borderView)
        borderView.frame = CGRect(x: horizontalPadding, y: verticalPadding + labelHeight, width: UIScreen.main.bounds.width - (2 * horizontalPadding), height: 2)
    }
    
}

extension SelectFormView: FormViewProtocol, FormViewValidatableProtocol {
    
    public func resetValues() {
        titleLabel.text = title
        detailLabel.text = value
    }
    
    public func getValue() -> String? {
        return self.value
    }
    
    public func getTitle() -> String {
        return self.title
    }
    
    public func selected() {
        isSelected = true
        UIView.animate(withDuration: 0.25) {
            self.changeAccentColor()
        }
    }
    
    public func deselected() {
        isSelected = false
        UIView.animate(withDuration: 0.25) {
            self.changeAccentColor()
        }
    }
    
    
    public func showError(error: String) {
        self.error = error
        addSubview(errorLabel)
        errorLabel.text = error
        errorLabel.textColor = Styles.global.errorColor
        errorLabel.textAlignment = Styles.global.textAlignment
        errorLabel.font = Styles.global.fontTiny
        errorLabel.frame = CGRect(x: horizontalPadding, y: verticalPadding + labelHeight + 20, width: UIScreen.main.bounds.width - (2 * horizontalPadding), height: errorHeight)
        changeAccentColor()
        invalidateIntrinsicContentSize()
    }
    
    public func clearError() {
        errorLabel.removeFromSuperview()
        invalidateIntrinsicContentSize()
    }
    
}
