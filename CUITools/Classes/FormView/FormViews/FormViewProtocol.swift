//
//  TextViewProtocol.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/4/1396 AP.
//

/**
 * Padding from sides: horizontal 16, vartical 8
 */

public protocol FormViewProtocol {
    
    func getValue() -> String?
    
    func resetValues()
    
    func selected()
    
    func deselected()
    
}

extension FormViewProtocol {
    
    public func showError(error: String) {}
    
    public func clearError() {}
    
    public func selected() {}
    
    public func deselected() {}
    
    public func reload(_ handler: (Self) -> Void) {
        handler(self)
        resetValues()
    }
}

public protocol FormViewValidatableProtocol: FormViewProtocol {
    
    func getTitle() -> String
    
    func showError(error: String)
    
    func clearError()
    
}
