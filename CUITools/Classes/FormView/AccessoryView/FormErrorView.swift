//
//  AccessoryViewProtocol.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/9/1396 AP.
//

import UIKit

public class FormErrorView: UIView {
    
    var error: String?
    
    let padding: CGFloat = 16
    let errorLabel = UILabel()
    let errorLableContainer = UILabel()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupContainerView()
        setupErrorLabel()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func set(error: String) {
        self.error = error
        self.errorLabel.text = error
        self.invalidateIntrinsicContentSize()
    }
    
    fileprivate func setupErrorLabel() {
        errorLableContainer.addSubview(errorLabel)
        errorLabel.numberOfLines = 10
        errorLabel.text = error
        errorLabel.textColor = Styles.global.errorColor
        errorLabel.font = Styles.global.fontNormal
        errorLabel.textAlignment = .center
        errorLabel.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self).inset(padding)
        }
    }
    
    fileprivate func setupContainerView() {
        addSubview(errorLableContainer)
        errorLableContainer.snp.makeConstraints { (make) in
            make.right.left.top.bottom.equalTo(self).inset(padding)
        }
        errorLableContainer.layer.shadowColor = UIColor.black.cgColor
        errorLableContainer.layer.shadowOffset = .zero
        errorLableContainer.layer.shadowRadius = 8
        errorLableContainer.layer.shadowOpacity = 0.1
    }
    
    override public var intrinsicContentSize: CGSize {
        guard let error = error else {
            return .zero
        }
        let labelSize = error.sizeInView(horizontalPadding: padding * 2, font: Styles.global.fontNormal)
        let size = CGSize(width: UIScreen.main.bounds.width, height: labelSize.height + (padding * 4))
        return size
    }
    
}

