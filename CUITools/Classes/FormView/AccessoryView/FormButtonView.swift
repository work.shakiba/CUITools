//
//  FormButtonView.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/13/1396 AP.
//

import UIKit

public class FormButtonView: UIControl {
    
    public let loadingButton: LoadingButton
    public var insets: UIEdgeInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
    
    public init(title: String, type: Int = 0) {
        self.loadingButton = LoadingButton(title: title)
        super.init(frame: .zero)
        setupLoadingView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupLoadingView() {
        loadingButton.isUserInteractionEnabled = false
        addSubview(loadingButton)
        loadingButton.snp.makeConstraints { (make) in
            make.centerX.centerY.equalTo(self)
        }
    }
    
    override public var intrinsicContentSize: CGSize {
        let size = CGSize(width: UIScreen.main.bounds.width, height: loadingButton.intrinsicContentSize.height + insets.top + insets.bottom)
        return size
    }
    
    public var isLoading: Bool {
        set {
            loadingButton.isLoading = newValue
        }
        get {
            return loadingButton.isLoading
        }
    }
    
    public override var isEnabled: Bool {
        set {
            self.isUserInteractionEnabled = newValue
            loadingButton.isEnabled = newValue
        }
        get {
            return loadingButton.isEnabled
        }
    }
    
}
