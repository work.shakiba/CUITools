//
//  AccessoryViewProtocol.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/5/1396 AP.
//

import Foundation

public class FormLabelView: UIView {
    
    public var title: String {
        didSet {
            onContentChanged()
        }
    }
    public var textColor: UIColor = Styles.global.textColor {
        didSet {
            onContentChanged()
        }
    }
    public var textAlignment: NSTextAlignment = Styles.global.textAlignment{
        didSet {
            onContentChanged()
        }
    }
    public var font: UIFont = Styles.global.fontNormal {
        didSet {
            onContentChanged()
        }
    }
    public var inset: UIEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8) {
        didSet {
            onContentChanged()
        }
    }
    
    let label = UILabel()
    
    public init(title: String) {
        self.title = title
        super.init(frame: .zero)
        setupLabel()
    }
    
    fileprivate func setupLabel() {
        addSubview(label)
        onContentChanged()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func onContentChanged() {
        label.textAlignment = textAlignment
        label.text = title
        label.textColor = textColor
        label.font = font
        label.numberOfLines = 99
        label.snp.removeConstraints()
        label.snp.makeConstraints { (make) in
            make.top.equalTo(self).inset(self.inset.top)
            make.bottom.equalTo(self).inset(self.inset.bottom)
            make.leading.trailing.equalTo(self).inset(self.inset.left)
        }
        invalidateIntrinsicContentSize()
    }
    
    override public var intrinsicContentSize: CGSize {
        let labelSize = title.sizeInView(horizontalPadding: inset.left, font: font)
        let size = CGSize(width: UIScreen.main.bounds.width - (inset.left + inset.right), height: labelSize.height + (inset.top + inset.bottom))
        return size
    }
    
    
}

