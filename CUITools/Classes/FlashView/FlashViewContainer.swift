//
//  FlashViewContainer.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/26/1396 AP.
//

import UIKit

final class FlashViewContainer: UIView {
    
    var provider: FlashViewProvider
    var flashView: FlashView
    var horizontalPadding: CGFloat = 16
    let verticalPadding: CGFloat = 16
    let animationDuration: TimeInterval = 2
    
    init(provider: FlashViewProvider) {
        self.provider = provider
        if provider.message != nil {
            self.horizontalPadding = 32
        }
        flashView = FlashView(provider: provider)
        super.init(frame: .zero)
        setupView()
        styleUI()
        animateUI()
    }
    
    override init(frame: CGRect) {
        fatalError("FlashView cannot be instaniated with just frame")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("FlashView cannot be instaniated from xib ")
    }
    
    fileprivate func setupView() {
        flashView.frame = CGRect(x: horizontalPadding, y: verticalPadding, width: flashView.intrinsicContentSize.width, height: flashView.intrinsicContentSize.height)
        addSubview(flashView)
    }
    
    fileprivate func styleUI() {
        self.alpha = 0
        self.alpha = 0
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 0.05
        self.layer.shadowRadius = 5
        self.backgroundColor = UIColor(white: 0.95, alpha: 1)
    }
    
    fileprivate func animateUI() {
        UIView.animateKeyframes(withDuration: animationDuration, delay: 0, options: [], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.05, animations: {
                self.alpha = 1
            })
            UIView.addKeyframe(withRelativeStartTime: 0.95, relativeDuration: 0.05, animations: {
                self.alpha = 0
            })
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        let flashViewSize = flashView.intrinsicContentSize
        return CGSize(width: flashViewSize.width + (2 * horizontalPadding), height: flashViewSize.height + (2 * verticalPadding))
    }
    
}
