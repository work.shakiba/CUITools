//
//  FlashViewImageProvider.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/26/1396 AP.
//

import UIKit

final class FlashViewImageProvider {
    
    func imageLayerForSuccess(frame: CGRect, lineWidth: CGFloat, color: CGColor) -> CAShapeLayer {
        let path = UIBezierPath()
        let height40 = frame.height * 0.4
        let height80 = frame.height * 0.8
        let width40 = frame.width * 0.4
        let height15 = frame.height * 0.15
        let point = CGPoint(x: 0, y: height40)
        let point2 = CGPoint(x: width40, y: height80)
        let point3 = CGPoint(x: frame.width, y: height15)
        path.move(to: point)
        path.addLine(to: point2)
        path.addLine(to: point3)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.lineJoin = "round"
        shapeLayer.lineCap = "round"
        shapeLayer.lineWidth = lineWidth
        shapeLayer.strokeColor = color
        shapeLayer.fillColor = UIColor.clear.cgColor
        return shapeLayer
    }
    
    func imageLayerForError(frame: CGRect, lineWidth: CGFloat, color: CGColor) -> CAShapeLayer {
        let path = UIBezierPath()
        let padding = frame.width / 8
        let point1 = CGPoint(x: padding, y: padding)
        let point2 = CGPoint(x: frame.width - (padding), y: frame.height - (padding))
        let point4 = CGPoint(x: padding, y: frame.height - (padding))
        let point3 = CGPoint(x: frame.width - (padding), y: padding)
        path.move(to: point1)
        path.addLine(to: point2)
        path.move(to: point3)
        path.addLine(to: point4)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.lineJoin = "round"
        shapeLayer.lineCap = "round"
        shapeLayer.lineWidth = lineWidth
        shapeLayer.strokeColor = color
        shapeLayer.fillColor = UIColor.clear.cgColor
        return shapeLayer
    }
    
}
