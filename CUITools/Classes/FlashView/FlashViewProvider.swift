//
//  FlashViewProvider.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/26/1396 AP.
//

import UIKit


public enum FlashViewType {
    case error
    case success
    case custom(UIImage)
}

public final class FlashViewProvider {
    
    public let message: String?
    public let type: FlashViewType
    
    public init(message: String?, type: FlashViewType) {
        self.message = message
        self.type = type
    }
    
    public convenience init(success: String?) {
        self.init(message: success, type: .success)
    }
    
    public convenience init(error: String?) {
        self.init(message: error, type: .error)
    }
    
    public func present(in viewController: UIViewController) {
        let view = FlashViewContainer(provider: self)
        viewController.view.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerXAnchor.constraint(equalTo: viewController.view.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: viewController.view.centerYAnchor).isActive = true
        
    }
    
    public func present() {
        let view = FlashViewContainer(provider: self)
        let window = UIApplication.shared.keyWindow!
        window.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
    }
    
}
