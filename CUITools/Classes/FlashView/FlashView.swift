//
//  FlashView.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/26/1396 AP.
//

import UIKit

final class FlashView: UIView, CAAnimationDelegate {
    
    var provider: FlashViewProvider
    var imageProvider: FlashViewImageProvider
    let imageSize = CGSize(width: 50, height: 50)
    let animationDuration: TimeInterval = 0.5
    let animationOffset: TimeInterval = 0.25
    
    var imageLayer: CAShapeLayer?
    
    var frameForImage: CGRect {
        let imagePadding = provider.message == nil ? 0 : ((finalWidth - imageSize.width) / 2)
        let imageFrame = CGRect(x: imagePadding, y: 0, width: imageSize.width, height: imageSize.height)
        return imageFrame
    }
    
    var finalWidth: CGFloat {
        let labelSize = sizeForLabel()
        let width = labelSize.width > imageSize.width ? labelSize.width : imageSize.width
        return width
    }
    
    init(provider: FlashViewProvider) {
        self.provider = provider
        self.imageProvider = FlashViewImageProvider()
        super.init(frame: .zero)
        setupView()
    }
    
    override init(frame: CGRect) {
        fatalError("FlashView cannot be instaniated with just frame")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("FlashView cannot be instaniated from xib ")
    }
    
    fileprivate func setupView() {
        switch self.provider.type {
        case .success: setupLayerForSuccessAnimation()
        case .error: setupLayerForFailedAnimation()
        case .custom(let image): setupImageView(image)
        }
        setupLabelIfNeeded()
    }
    
    fileprivate func animateLayer() {
        guard let shapeLayer = self.imageLayer else { return }
        shapeLayer.opacity = 0
        CATransaction.begin()
        
//        let alphaAnimation = CABasicAnimation(keyPath: "opacity")
//        alphaAnimation.fromValue = 0
//        alphaAnimation.toValue = 1
//        alphaAnimation.duration = 0.1
//        shapeLayer.add(alphaAnimation, forKey: "AlphaAnimation")
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = animationDuration
        shapeLayer.add(animation, forKey: "StrokeAnimation")
        shapeLayer.strokeEnd = 1
        shapeLayer.opacity = 1
        CATransaction.commit()
    }
    
    fileprivate func setupLayerForSuccessAnimation() {
        let color = UIColor(hex: "019142")
        imageLayer = imageProvider.imageLayerForSuccess(frame: frameForImage, lineWidth: 5, color: color.cgColor)
        imageLayer?.frame = frameForImage
        self.layer.addSublayer(imageLayer!)
        animateLayer()
    }

    
    fileprivate func setupLayerForFailedAnimation() {
        let color = UIColor(hex: "ff4d4d")
        imageLayer = imageProvider.imageLayerForError(frame: frameForImage, lineWidth: 5, color: color.cgColor)
        imageLayer?.frame = frameForImage
        imageLayer?.strokeEnd = 0
        self.layer.addSublayer(imageLayer!)
        animateLayer()
    }
    
    fileprivate func setupImageView(_ image: UIImage) {
        let imageView = UIImageView()
        imageView.frame = frameForImage
        imageView.image = image
        self.addSubview(imageView)
    }
    
    fileprivate func setupLabelIfNeeded() {
        guard let message = self.provider.message else { return }
        let label = UILabel()
        label.textColor = Styles.global.lightTextColor
        label.font = Styles.global.fontNormal
        label.numberOfLines = 99
        label.text = message
        label.textAlignment = .center
        addSubview(label)
        let labelSize = sizeForLabel()
        label.frame = CGRect(x: 0, y: imageSize.height + 8, width: finalWidth, height: labelSize.height)
    }
    
    fileprivate func sizeForLabel() -> CGSize {
        guard let message = self.provider.message else { return .zero }
        let label = UILabel()
        label.text = message
        label.font = Styles.global.fontNormal
        label.numberOfLines = 99
        let size = CGSize(width: imageSize.width * 2, height: .infinity)
        let labelSize = label.sizeThatFits(size)
        return labelSize
    }
    
    override var intrinsicContentSize: CGSize {
        if provider.message == nil {
            return imageSize
        }else{
            let labelSize = sizeForLabel()
            let height = imageSize.height + labelSize.height + 8
            return CGSize(width: finalWidth, height: height)
        }
        
    }
    
}


