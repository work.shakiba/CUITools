//
//  SelectImageProtocol.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/1/1396 AP.
//

import UIKit
import DKImagePickerController

public struct SelectImageConfig {
    
    public var selectionCount: Int = 1
    public var sourceType: DKImagePickerControllerSourceType = .both
    public var showCancelButton: Bool = true
    
    public init() {
        
    }
    
}

public protocol SelectImageProtocol {
    
    func imageSelectionSucceed(images: [UIImage])
    func imageSelectionCanceled()
    
}

extension SelectImageProtocol {
    
    public func presentSelectImage(config: SelectImageConfig) {
        
        let pickerController = DKImagePickerController()
        pickerController.maxSelectableCount = config.selectionCount
        pickerController.sourceType = config.sourceType
        pickerController.showsCancelButton = config.showCancelButton
        if config.selectionCount == 1 {
            pickerController.singleSelect = true
        }else{
            pickerController.singleSelect = false
        }
        pickerController.didSelectAssets = { (assets: [DKAsset]) in
            var images: [UIImage] = []
            assets.forEach({ (asset) in
                asset.fetchImageDataForAsset(true, completeBlock: { (data, _) in
                    guard let data = data, let image = UIImage(data: data) else { return }
                    images.append(image)
                })
            })
            self.imageSelectionSucceed(images: images)
        }
        pickerController.didCancel = {
            self.imageSelectionCanceled()
        }
        guard let selfViewController = self as? UIViewController else {
            return
        }
        selfViewController.present(pickerController, animated: true) {}
    }
    
}
