//
//  SelectOptionProvider.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/23/1396 AP.
//

import Foundation

/**
 * contains the result for SelectOptionViewController
 **/
public enum SelectOptionResult<T> {
    case result(T)
    case cancel
}

/**
 * this class will contain the values used for SelectOptionController
 * the title is showed to the user and the value will be returned in the callback
 * the user can only select one option
 **/
public class SelectOptionProvider<T> {
    
    /**
     * this is the internal structure for storing items in the order they were added
     **/
    struct OptionValue<T> {
        let title: String
        let value: T
    }
    
    var items: [OptionValue<T>] = []
    // handler for result, must be removed
    var resultHandler: ((SelectOptionResult<T>) -> Void)?
    
    public private(set) var headerTitle: String?
    public private(set) var cancelTitle: String
    
    /// default initializer
    /// the headerTitle is user to show at the top other options
    /// the cancel button indicates the string of button for canceling the SelectOptionViewController
    public init(headerTitle: String? = nil, cancelTitle: String) {
        self.headerTitle = headerTitle
        self.cancelTitle = cancelTitle
    }
    
    /// MARK: result methods
    
    func onResult(title: String) {
        guard let item = self.items.first(where: {$0.title == title}) else { return }
        resultHandler?(.result(item.value))
        self.resultHandler = nil
    }
    
    func onCancel() {
        resultHandler?(.cancel)
        resultHandler = nil
    }
    
    /// MARK: public methods
    
    /// append item to the end of array
    public func appendItem(title: String, value: T) {
        let option = OptionValue<T>(title: title, value: value)
        items.append(option)
    }
    
    /// insert item at a specific location
    public func insertItem(title: String, value: T, at: Int) {
        let option = OptionValue<T>(title: title, value: value)
        items.insert(option, at: at)
    }
    
    /// get value at a specific location
    public func getItem(at index: Int) -> T {
        return items[index].value
    }
    
    /// this handler is called when the user selects an option or cancels the select controller
    public func onResult(_ handler: @escaping (SelectOptionResult<T>) -> Void) {
        self.resultHandler = handler
    }
    
    /// call this method to present the option view controller in the given controller
    public func present(in viewController: UIViewController) {
        let vc = SelectOptionViewController(provider: self)
        viewController.present(vc, animated: true, completion: nil)
    }
    
}
