//
//  SelectOptionViewController.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/23/1396 AP.
//

import UIKit

class SelectOptionView: UIControl {
    
    var title: String
    var label = UILabel()
    
    init(title: String) {
        self.title = title
        super.init(frame: .zero)
        backgroundColor = .white
        setupSubview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSubview() {
        addSubview(label)
        label.snp.makeConstraints { (make) in
            make.left.right.equalTo(self).inset(16)
            make.top.bottom.equalTo(self).inset(12)
        }
        label.text = title
        label.numberOfLines = 99
    }
    
    override func layoutSubviews() {
        addBorderIfNeeded(.bottom, color: Styles.global.borderColor)
        super.layoutSubviews()
    }
    
    func styleForItem() {
        label.font = Styles.global.fontNormal
        label.textAlignment = .center
        label.textColor = Styles.global.lightTextColor
    }
    
    func styleForCancel() {
        label.font = Styles.global.fontNormal
        label.textAlignment = .center
        label.textColor = Styles.global.errorColor
    }
    
    func styleForTitle() {
        label.font = Styles.global.fontNormal
        label.textAlignment = .right
        label.textColor = Styles.global.textColor
    }
    
    override var intrinsicContentSize: CGSize {
        let labelSize = title.sizeInView(horizontalPadding: 16, font: Styles.global.fontNormal)
        return CGSize(width: UIScreen.main.bounds.width, height: labelSize.height + 24)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        label.alpha = 0.35
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        label.alpha = 1
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        label.alpha = 1
    }
    
}

class SelectOptionViewController<T>: UIViewController, UIViewControllerTransitioningDelegate, GenericTransitioningAnimationDelegate {
    
    
    var provider: SelectOptionProvider<T>
    let stackView = UIStackView()
    let viewOverlay = UIView()

    
    /// MARK: initializer
    
    init(provider: SelectOptionProvider<T>) {
        self.provider = provider
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overFullScreen
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// MARK: lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOverlay()
        setupSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /// MARK: add subviews
    
    fileprivate func setupOverlay() {
        view.addSubview(viewOverlay)
        viewOverlay.backgroundColor = UIColor(white: 0, alpha: 0.3)
        viewOverlay.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onCancelClicked))
        viewOverlay.addGestureRecognizer(gestureRecognizer)
    }
    
    /// add arranges subviews
    fileprivate func setupSubviews() {
        // add subview
        stackView.axis = .vertical
        stackView.backgroundColor = .white
        self.view.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.width.equalTo(self.view)
        }
        // add items
        for item in provider.items {
            let view = SelectOptionView(title: item.title)
            view.styleForItem()
            view.addTarget(self, action: #selector(onItemClicked(_:)), for: .touchUpInside)
            stackView.addArrangedSubview(view)
        }
        // add cancel button
        let view = SelectOptionView(title: provider.cancelTitle)
        view.styleForCancel()
        view.addTarget(self, action: #selector(onCancelClicked), for: .touchUpInside)
        stackView.addArrangedSubview(view)
        // add header
        if let title = provider.headerTitle {
            let view = SelectOptionView(title: title)
            view.styleForTitle()
            stackView.insertArrangedSubview(view, at: 0)
        }
    }
    
    /// MARK: on click actions
    
    func onItemClicked(_ sender: SelectOptionView) {
        provider.onResult(title: sender.title)
        dismiss(animated: true, completion: nil)
    }
    
    func onCancelClicked() {
        provider.onCancel()
        dismiss(animated: true, completion: nil)
    }
    
    /// MARK: animations
    
    func animateForPresentation(timeInterval: TimeInterval, handler: @escaping () -> Void) {
        viewOverlay.alpha = 0
        view.setNeedsLayout()
        view.layoutIfNeeded()
        let stackViewSize = self.stackView.bounds.height
        self.stackView.transform = CGAffineTransform(translationX: 0, y: stackViewSize)
        UIView.animate(withDuration: timeInterval, animations: {
            self.stackView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.viewOverlay.alpha = 1
        }) { (_) in
             self.stackView.transform = .identity
            handler()
        }
    }
    
    func animateForDismissal(timeInterval: TimeInterval, handler: @escaping () -> Void) {
        UIView.animate(withDuration: timeInterval, animations: {
            let stackViewSize = self.stackView.bounds.height
            self.stackView.transform = CGAffineTransform(translationX: 0, y: stackViewSize)
            self.viewOverlay.alpha = 0
        }) { (_) in
            handler()
        }
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = GenericTransitioningAnimation(isPresenting: true)
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = GenericTransitioningAnimation(isPresenting: false)
        return transition
    }
    
}
