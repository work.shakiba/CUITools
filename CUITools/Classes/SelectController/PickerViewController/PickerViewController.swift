//
//  PickerViewController.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/23/1396 AP.
//

import UIKit

class PickerViewController: UIViewController {
    
    let provider: PickerProvider
    let pickerView = UIPickerView()
    let stackView = UIStackView()
    let overlayView = UIView()
    
    init(provider: PickerProvider) {
        self.provider = provider
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overFullScreen
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOverlay()
        setupStackView()
        setupHeaderView()
        setupPickerView()
        setupCancelButton()
    }
    
    fileprivate func setupOverlay() {
        view.addSubview(overlayView)
        overlayView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        overlayView.backgroundColor = UIColor(white: 0, alpha: 0.3)
    }
    
    fileprivate func setupStackView() {
        stackView.axis = .vertical
        stackView.spacing = 16
        view.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
        }
    }
    
    fileprivate func setupPickerView() {
        stackView.addArrangedSubview(pickerView)
        pickerView.delegate = self
        pickerView.snp.makeConstraints { (make) in
            make.bottom.left.right.equalTo(self.view)
            make.height.equalTo(250)
        }
    }
    
    fileprivate func setupHeaderView() {
        guard let headerTitle = provider.headerTitle else { return }
        let headerView = PickerHeaderView(title: headerTitle)
        stackView.addArrangedSubview(headerView)
        
    }
    
    fileprivate func setupCancelButton() {
        let cancelButton = UIButton()
        stackView.addArrangedSubview(cancelButton)
        cancelButton.addTarget(self, action: #selector(onCancel), for: .touchUpInside)
        cancelButton.setTitle(provider.cancelTitle, for: .normal)
        cancelButton.titleLabel?.font = Styles.global.fontNormal
        cancelButton.titleLabel?.textColor = Styles.global.errorColor
        cancelButton.snp.makeConstraints { (make) in
            make.height.equalTo(50)
        }
    }
    
    internal func onResult() {
       self.dismiss(animated: true, completion: nil)
    }
    
    internal func onCancel() {
        provider.onCancel()
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension PickerViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = GenericTransitioningAnimation(isPresenting: true)
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = GenericTransitioningAnimation(isPresenting: false)
        return transition
    }
    
}

extension PickerViewController: GenericTransitioningAnimationDelegate {
    
    func animateForPresentation(timeInterval: TimeInterval, handler: @escaping () -> Void) {
        view.setNeedsLayout()
        view.layoutIfNeeded()
        let sizeOfStackView = stackView.bounds.height
        stackView.transform = CGAffineTransform(translationX: 0, y: sizeOfStackView)
        UIView.animate(withDuration: timeInterval, animations: {
            self.stackView.transform = CGAffineTransform.identity
        }) { (_) in
            handler()
        }
    }
    
    func animateForDismissal(timeInterval: TimeInterval, handler: @escaping () -> Void) {
        let sizeOfStackView = stackView.bounds.height
        UIView.animate(withDuration: timeInterval, animations: {
            self.stackView.transform = CGAffineTransform(translationX: 0, y: sizeOfStackView)
        }) { (_) in
            handler()
        }
    }
    
}

extension PickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return provider.items.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return provider.items[component]?.count ?? 0
    }
    
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let component = provider.items[component]
        let title = component![row].title
        if let titleLabel = view as? UILabel {
            titleLabel.text = title
            return titleLabel
        } else {
            let titleLabel = UILabel()
            titleLabel.textAlignment = .center
            titleLabel.font = Styles.global.fontNormal
            titleLabel.textColor = Styles.global.lightTextColor
            titleLabel.text = title
            return titleLabel
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    
    
}
