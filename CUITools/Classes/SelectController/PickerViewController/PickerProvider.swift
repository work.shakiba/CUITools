//
//  PickerProvider.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/23/1396 AP.
//

import Foundation

/**
 * contains the result for SelectOptionViewController
 **/
public enum PickerResult {
    case result([Int: Any])
    case cancel
}

/**
 * PickerProvider stores data for presenting in the picker view
 */
public class PickerProvider {
    
    /**
     * contains data for PickerProvider
     */
    struct PickerValue {
        var title: String
        var value: Any
    }
    
    var items: [Int: [PickerValue]] = [:]
    var onResultHandler: ((PickerResult) -> Void)?
    let cancelTitle: String
    let headerTitle: String?
    
    public init(headerTitle: String?, cancelTitle: String) {
        self.headerTitle = headerTitle
        self.cancelTitle = cancelTitle
    }
    
    /**
     * add item to the component
     */
    public func addItem(_ title: String, value: Any, component: Int) {
        if items[component] == nil {
            items[component] = []
        }
        let item = PickerValue(title: title, value: value)
        items[component]?.append(item)
    }
    
    /**
     * set the handler for onResult
     * this handler will be called once the user selects done or cancels the process
     */
    public func onResult(_ handler: @escaping (PickerResult) -> Void ) {
        self.onResultHandler = handler
    }
    
    func onResult(values: [Int: Any]) {
        onResultHandler?(.result(values))
    }
    
    func onCancel() {
        onResultHandler?(.cancel)
    }
    
    /// call this method to present the option view controller in the given controller
    public func present(in viewController: UIViewController) {
        let vc = PickerViewController(provider: self)
        viewController.present(vc, animated: true, completion: nil)
    }
    
}
