//
//  PickerHeaderView.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/23/1396 AP.
//

import UIKit

class PickerHeaderView: UIView {
    
    let title: String
    let label = UILabel()
    let button = UIButton()
    let doneButtonSize = CGSize(width: 30, height: 30)
    
    init(title: String) {
        self.title = title
        super.init(frame: .zero)
        setupLable()
        setupDoneButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    fileprivate func setupLable() {
        label.text = title
        label.font = Styles.global.fontNormal
        label.textColor = Styles.global.textColor
        label.textAlignment = .right
        addSubview(label)
        label.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(self).inset(16)
            make.right.equalTo(self).inset(16)
            make.left.equalTo(16 + doneButtonSize.width + 16)
        }
    }
    
    fileprivate func setupDoneButton() {
    }
    
    override var intrinsicContentSize: CGSize {
        let sizeForLabel = title.sizeInView(left: 16, right: doneButtonSize.width + 32, font: Styles.global.fontNormal)
        return CGSize(width: UIScreen.main.bounds.width, height: sizeForLabel.height)
    }
    
}
