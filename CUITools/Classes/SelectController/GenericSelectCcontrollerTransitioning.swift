//
//  GenericSelectCcontrollerTransitioning.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/23/1396 AP.
//

import Foundation

protocol GenericTransitioningAnimationDelegate {
    
    func animateForPresentation(timeInterval: TimeInterval, handler: @escaping () -> Void)
    
    func animateForDismissal(timeInterval: TimeInterval, handler: @escaping () -> Void)
    
}


class GenericTransitioningAnimation: NSObject, UIViewControllerAnimatedTransitioning {
    
    let timeInterval: TimeInterval = 0.3
    let isPresenting: Bool
    
    init(isPresenting: Bool) {
        self.isPresenting = isPresenting
        super.init()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return timeInterval
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if isPresenting {
            animateForPresentation(transitionContext: transitionContext)
        }else{
            animateForDismissal(transitionContext: transitionContext)
        }
    }
    
    fileprivate func animateForPresentation(transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
//        let fromVC = transitionContext.viewController(forKey: .from)!
//        let fromView = transitionContext.view(forKey: .from)!
        let toVC = transitionContext.viewController(forKey: .to)!
        let toView = transitionContext.view(forKey: .to)!
        containerView.addSubview(toView)
        if let delegate = toVC as? GenericTransitioningAnimationDelegate {
            delegate.animateForPresentation(timeInterval: timeInterval, handler: {
                transitionContext.completeTransition(true)
            })
        }
    }
    
    fileprivate func animateForDismissal(transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
                let fromVC = transitionContext.viewController(forKey: .from)!
//                let fromView = transitionContext.view(forKey: .from)!
//        let toVC = transitionContext.viewController(forKey: .to)!
//        let toView = transitionContext.view(forKey: .to)!
        if let delegate = fromVC as? GenericTransitioningAnimationDelegate {
            delegate.animateForDismissal(timeInterval: timeInterval, handler: {
                transitionContext.completeTransition(true)
            })
        }
    }
    
}
