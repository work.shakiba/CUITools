//
//  NotificationView.swift
//  CUITools
//
//  Created by mohsen shakiba on 8/1/1396 AP.
//

import UIKit
import SnapKit

public enum NotificationType {
    case success
    case error
    case warning
    
    var color: UIColor {
        switch self {
        case .error: return UIColor(hex: "ff7373")
        case .success: return UIColor(hex: "2ecc71")
        case .warning: return UIColor(hex: "f39c12")
        }
    }
    
}

public class NotificationProvider {
    
    let text: String
    let type: NotificationType
    let horizontalPadding: CGFloat = 16
    let topPadding: CGFloat = 28
    
    public init(text: String, type: NotificationType = NotificationType.success) {
        self.text = text
        self.type = type
    }
    
    public func present(in viewController: UIViewController) {
        let notificationView = NotificationView(provider: self)
        viewController.view.addSubview(notificationView)
        animateNotificationView(notificationView)
        notificationView.snp.makeConstraints { (make) in
            make.top.equalTo(viewController.view).inset(topPadding)
            make.left.right.equalTo(viewController.view).inset(horizontalPadding)
        }
    }
    
    public func presentInWindow() {
        guard let window = UIApplication.shared.keyWindow else { return }
        let notificationView = NotificationView(provider: self)
        window.addSubview(notificationView)
        animateNotificationView(notificationView)
        notificationView.snp.makeConstraints { (make) in
            make.top.equalTo(window).inset(topPadding)
            make.left.right.equalTo(window).inset(horizontalPadding)
        }
    }
    
    fileprivate func animateNotificationView(_ notificationView: NotificationView) {
        let originalTransform = CGAffineTransform(translationX: 0, y: -(notificationView.intrinsicContentSize.height + topPadding))
        notificationView.transform = originalTransform
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseOut], animations: {
            notificationView.transform = CGAffineTransform.identity
        }, completion: nil)
        UIView.animate(withDuration: 0.2, delay: 3, options: [.curveEaseOut], animations: {
            notificationView.transform = originalTransform
        }, completion: {_ in
            notificationView.removeFromSuperview()
        })
    }
    
}

class NotificationView: UIView {
    
    let containerView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
    let titleLabel = UILabel()
    let indicatorView = UIView()
    
    let provider: NotificationProvider
    
    init(provider: NotificationProvider) {
        self.provider = provider
        super.init(frame: .zero)
        self.backgroundColor = .clear
        setupSubviews()
        addShadow()
        titleLabel.text = provider.text
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func setupSubviews() {
        addSubview(containerView)
        containerView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        containerView.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.right.equalTo(containerView).inset(19)
            make.left.equalTo(containerView).inset(13)
            make.top.bottom.equalTo(containerView).inset(16)
        }
        titleLabel.font = Styles.global.fontNormal
        titleLabel.textAlignment = Styles.global.textAlignment
        titleLabel.textColor = Styles.global.textColor
        containerView.contentView.addSubview(indicatorView)
        indicatorView.snp.makeConstraints { (make) in
            make.top.right.bottom.equalTo(self.containerView)
            make.width.equalTo(3)
        }
        indicatorView.backgroundColor = provider.type.color
        containerView.layer.cornerRadius = 3
        containerView.layer.masksToBounds = true
    }
    
    func addShadow() {
        self.layer.cornerRadius = 3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.15
    }
    
    override var intrinsicContentSize: CGSize {
        let sizeOfText = provider.text.sizeInView(horizontalPadding: provider.horizontalPadding, font: Styles.global.fontNormal)
        return CGSize(width: UIScreen.main.bounds.width - ((2 * provider.horizontalPadding) + 32), height: sizeOfText.height + 32)
    }
    
}

