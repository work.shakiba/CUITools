//
//  BottomAlertProvider.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/24/1396 AP.
//

import Foundation

/**
 * display a new BulletinAlertProvider
 */
public class BulletinAlertProvider {
    
    /// MARK: public get-only properties
    
    /// title for the given provider
    public let title: String
    /// detail for the given provider
    public let detail: String?
    /// image for the given provider
    public let image: UIImage?
    /// clickable action for the given provider
    public let actionTitle: String?
    /// cancel button title for the given provider
    public let cancelTitle: String?
    /// size for the image in page
    public var size: CGSize = CGSize(width: 100, height: 100)
    /// delegate, notifies of cancel or action
    public weak var delegate: BulletinAlertDelegate?

    /// internal properties
    
    weak var viewController: UIViewController?
    
    /// default initializer
    public init(title: String, detail: String, image: UIImage? = nil, actionTitle: String?, cancelTitle: String?) {
        self.title = title
        self.detail = detail
        self.image = image
        self.actionTitle = actionTitle
        self.cancelTitle = cancelTitle
    }
    
    /// will dismiss the controller asoociated with the provider
    public func dismiss() {
        viewController?.dismiss(animated: true, completion: nil)
    }
    
    public func present(in viewController: UIViewController) {
        let vc = BulletingAlertViewController(provider: self)
        viewController.present(vc, animated: true, completion: nil)
        self.viewController = vc
    }
    
    func onAction() {
        delegate?.bulletin(actionClicked: self)
    }
    
    func onCancel() {
        dismiss()
        delegate?.bulletin(cancelClicked: self)
    }

    
}

public protocol BulletinAlertDelegate: class {
    
    func bulletin(actionClicked provider: BulletinAlertProvider)
    func bulletin(cancelClicked provider: BulletinAlertProvider)
    
}
