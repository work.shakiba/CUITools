//
//  BulettinAlertViewController.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/24/1396 AP.
//

import UIKit
import SnapKit

class BulletingAlertViewController: UIViewController {
    
    /// MARK: Properties
    
    let provider: BulletinAlertProvider
    
    /// MARK: UI Properties
    
    let overlayView = UIView()
    let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
    let scrollView = UIScrollView()
    let stackView = UIStackView()
    let titleLabel = UILabel()
    let detailLabel = UILabel()
    let imageView = UIImageView()
    let actionButton = UIButton()
    let cancelButton = UIButton()
    
    
    init(provider: BulletinAlertProvider) {
        self.provider = provider
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overCurrentContext
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOverlayView()
        setupVisualEffectView()
        setupStackView()
        setupImageView()
        setupTitleLabel()
        setupDetaillabel()
        setupActionButton()
        setupCancelButton()
    }

    fileprivate func setupOverlayView() {
        view.addSubview(overlayView)
        overlayView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        overlayView.backgroundColor = UIColor(white: 0, alpha: 0.3)
    }
    
    fileprivate func setupVisualEffectView() {
        view.addSubview(visualEffectView)
        visualEffectView.layer.cornerRadius = 16
        visualEffectView.layer.masksToBounds = true
        visualEffectView.snp.makeConstraints { (make) in
            make.bottom.left.right.equalTo(self.view).inset(24)
        }
    }
    
    fileprivate func setupStackView() {
        visualEffectView.contentView.addSubview(stackView)
        stackView.axis = .vertical
        stackView.spacing = 12
        stackView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.visualEffectView.contentView).inset(20)
        }
    }
    
    fileprivate func setupImageView() {
        guard let image = provider.image else { return }
        imageView.image = image
        stackView.addArrangedSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.height.equalTo(provider.size.height)
            make.width.equalTo(provider.size.width)
        }
    }
    
    fileprivate func setupTitleLabel() {
        titleLabel.text = provider.title
        titleLabel.textAlignment = .center
        titleLabel.textColor = Styles.global.textColor
        titleLabel.font = Styles.global.fontLarge
        titleLabel.numberOfLines = 99
        stackView.addArrangedSubview(titleLabel)
    }
    
    fileprivate func setupDetaillabel() {
        guard let detail = provider.detail else { return }
        detailLabel.text = detail
        detailLabel.textAlignment = .center
        detailLabel.textColor = Styles.global.lightTextColor
        detailLabel.font = Styles.global.fontNormal
        detailLabel.numberOfLines = 99
        stackView.addArrangedSubview(detailLabel)
    }
    
    fileprivate func setupActionButton() {
        guard let actionTitle = provider.actionTitle else { return }
        actionButton.setTitle(actionTitle, for: .normal)
        actionButton.setTitleColor(.white, for: .normal)
        actionButton.backgroundColor = Styles.global.accentColor
        actionButton.layer.cornerRadius = 5
        actionButton.titleLabel?.font = Styles.global.fontNormal
        actionButton.clipsToBounds = true
        actionButton.addTarget(self, action: #selector(onAction), for: .touchUpInside)
        stackView.addArrangedSubview(actionButton)
    }
    
    fileprivate func setupCancelButton() {
        guard let cancelTitle = provider.cancelTitle else { return }
        cancelButton.setTitle(cancelTitle, for: .normal)
        cancelButton.titleLabel?.font = Styles.global.fontNormal
        cancelButton.setTitleColor(Styles.global.errorColor, for: .normal)
        cancelButton.addTarget(self, action: #selector(onCancel), for: .touchUpInside)
        stackView.addArrangedSubview(cancelButton)
    }
    
    internal func onAction() {
        provider.onAction()
    }
    
    internal func onCancel() {
        provider.onCancel()
    }
    
}

extension BulletingAlertViewController: GenericTransitioningAnimationDelegate {
    
    func animateForPresentation(timeInterval: TimeInterval, handler: @escaping () -> Void) {
        view.setNeedsLayout()
        view.layoutIfNeeded()
        let heightOfVisualEffectView = visualEffectView.contentView.bounds.height
        visualEffectView.transform = CGAffineTransform(translationX: 0, y: heightOfVisualEffectView + 48)
        self.overlayView.alpha = 0
        UIView.animate(withDuration: timeInterval, animations: {
            self.visualEffectView.transform = .identity
            self.overlayView.alpha = 1
        }) { (_) in
            handler()
        }
    }
    
    func animateForDismissal(timeInterval: TimeInterval, handler: @escaping () -> Void) {
        let heightOfVisualEffectView = visualEffectView.contentView.bounds.height
        UIView.animate(withDuration: timeInterval, animations: {
            self.visualEffectView.transform = CGAffineTransform(translationX: 0, y: (heightOfVisualEffectView + 48))
            self.overlayView.alpha = 0
        }) { (_) in
            handler()
        }
    }
    
}

extension BulletingAlertViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return GenericTransitioningAnimation(isPresenting: false)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return GenericTransitioningAnimation(isPresenting: true)
    }
    
}
