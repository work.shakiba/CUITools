//
//  UITableView+Dequeue&Register.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/29/1396 AP.
//

import UIKit

extension UITableView {
    
    func register(cell: UITableViewCell.Type) {
        let identifer = String(describing: cell)
        let nib = UINib(nibName: identifer, bundle: nil)
        self.register(nib, forCellReuseIdentifier: identifer)
    }
    
    func dequeue<T>(cell: UITableViewCell.Type) -> T {
        let identifer = String(describing: cell)
        let cellDequeued = self.dequeueReusableCell(withIdentifier: identifer) as! T
        return cellDequeued
    }
    
}
