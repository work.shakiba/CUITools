//
//  UIViewController+Loading.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/29/1396 AP.
//

import UIKit

extension UIViewController {
    
    var tagForLoadingOverlay: Int {
        return 11456
    }
    
    public func startLoading() {
        let loadingView = LoadingView(type: .normal, color: .default)
        loadingView.sizeToFit()
        let loadingContainer = UIView()
        loadingContainer.backgroundColor = UIColor.white
        loadingContainer.layer.cornerRadius = 5
        loadingContainer.addShadow(radius: 5)
        let loadingViewSize = loadingView.intrinsicContentSize
        let addedSize = CGSize(width: loadingViewSize.width + 50, height: loadingViewSize.height + 50)
        loadingContainer.frame = CGRect(origin: .zero, size: addedSize)
        loadingContainer.addSubview(loadingView)
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.centerXAnchor.constraint(equalTo: loadingContainer.centerXAnchor).isActive = true
        loadingView.centerYAnchor.constraint(equalTo: loadingContainer.centerYAnchor).isActive = true
        loadingContainer.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        let overlay = UIView()
        loadingContainer.center = overlay.center
        overlay.addSubview(loadingContainer)
        view.addSubview(overlay)
        overlay.backgroundColor = UIColor(white: 0, alpha: 0.1)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        overlay.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        overlay.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        overlay.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        overlay.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        overlay.tag = tagForLoadingOverlay
        overlay.alpha = 0
        UIView.animate(withDuration: 0.25) {
            overlay.alpha = 1
        }
    }
    
    public func stopLoading() {
        
        view.subviews.forEach { (view) in
            if view.tag == tagForLoadingOverlay {
                removeViewWilAlphaAnimation(view)
            }
        }
    }
    
    fileprivate func removeViewWilAlphaAnimation(_ view: UIView) {
        UIView.animate(withDuration: 0.25, animations: {
            view.alpha = 0
        }) { (_) in
            view.removeFromSuperview()
        }
    }
    
}
