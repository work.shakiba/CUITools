//
//  Bundle+assets.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/28/1396 AP.
//

import UIKit

extension Bundle {
    
    static var assetsBundle: Bundle {
        let podBundle = Bundle(for: Styles.self)
        let url = podBundle.url(forResource: "CUITools", withExtension: "bundle")!
        let bundle = Bundle(url: url)
        return bundle!
    }
    
}


