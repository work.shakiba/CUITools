//
//  UILabel+Size.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/28/1396 AP.
//

import UIKit

extension String {
    
    public func sizeInView(horizontalPadding: CGFloat, font: UIFont) -> CGSize {
        let label = UILabel()
        label.numberOfLines = 99
        label.text = self
        label.font = font
        let screenSize = CGSize(width: UIScreen.main.bounds.width - (2 * horizontalPadding), height: .infinity)
        return label.sizeThatFits(screenSize)
    }
    
    public func sizeInView(left: CGFloat, right: CGFloat, font: UIFont) -> CGSize {
        let label = UILabel()
        label.numberOfLines = 99
        label.text = self
        label.font = font
        let screenSize = CGSize(width: UIScreen.main.bounds.width - (left + right), height: .infinity)
        return label.sizeThatFits(screenSize)
    }
    
    public func sizeInView(font: UIFont) -> CGSize {
        return self.sizeInView(horizontalPadding: 0, font: font)
    }
    
    public func sizeInView() -> CGSize {
        return self.sizeInView(horizontalPadding: 0, font: Styles.global.fontNormal)
    }
    
}
