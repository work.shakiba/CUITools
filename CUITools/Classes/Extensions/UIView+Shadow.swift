//
//  UIView+Shadow.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/29/1396 AP.
//

import UIKit

extension UIView {
    
    func addShadow(radius: CGFloat, offset: CGSize = .zero, alpha: Float = 0.1) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = alpha
    }
    
}
