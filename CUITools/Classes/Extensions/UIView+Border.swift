//
//  UIView+Border.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/29/1396 AP.
//

import UIKit

extension UIView {
    
    func addBorderIfNeeded(_ edge: UIRectEdge, color: UIColor, width: CGFloat = 1) {
        
        let name = "border-Layer" + String(edge.rawValue)
        
        // remove layer for border
        layer.sublayers?.forEach { layer in
            if layer.name == name {
                layer.removeFromSuperlayer()
            }
        }
        
        let px = 1 / UIScreen.main.scale
        let thickness: CGFloat = (px * width)
        let borderColor = color
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect.init(x: 0, y: 0, width: bounds.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect.init(x: 0, y: bounds.height - thickness, width: bounds.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect.init(x: 0, y: 0, width: thickness, height: bounds.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect.init(x: bounds.width - thickness, y: 0, width: thickness, height: bounds.height)
            break
        default:
            break
        }
        
        border.backgroundColor = borderColor.cgColor;
        border.name = name
        self.layer.addSublayer(border)
        
    }
    
}
