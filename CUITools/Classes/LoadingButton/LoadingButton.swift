//
//  LoadingButton.swift
//  CUITools
//
//  Created by mohsen shakiba on 7/29/1396 AP.
//

import UIKit

public class LoadingButton: UIControl {
    
    /// MARK: public properties
    
    /// indicates weather if button is in loading state or not
    @IBInspectable
    public var isLoading: Bool = false {
        didSet {
            animateStyleChange()
        }
    }
    
    /// indicates the type of button, can accept any of the LoadingButtonType values
    /// default value is LoadingButtonType.default
    @IBInspectable
    public var type: Int {
        didSet {
            self.buttonType = LoadingButtonType(rawValue: type)!
            setupSubviews()
        }
    }
    
    /// the title, representing the string for label
    @IBInspectable
    public var title: String {
        didSet {
            setupSubviews()
        }
    }
    
    /// is enabled, indicates if the button is clickable or not
    /// if the button is disabled, then setting the isLoading propery will cause it to be enabled again
    @IBInspectable
    public override var isEnabled: Bool {
        didSet {
            isUserInteractionEnabled = isEnabled
            setupSubviews()
        }
    }
    
    /// the insets for the label to button
    /// changing it will cause the view to perform invalidate intrinsic size
    @IBInspectable
    public var contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16) {
        didSet {
            setupSubviews()
        }
    }
    
    /// font used fot title label
    @IBInspectable
    public var titleLabelFont: UIFont = Styles.global.fontNormal {
        didSet {
            setupSubviews()
        }
    }
    
    /// this is the label used for presenting the text in the button
    public let label = UILabel()
    
    /// MARK: private properties
    
    /// internal poperties
    internal var loadingView: LoadingView!
    internal var isInErrorMode = false
    internal var buttonType: LoadingButtonType = .default
    
    /// MARK: initializer
    
    public init(title: String, type: LoadingButtonType = LoadingButtonType.default) {
        self.title = title
        self.type = type.rawValue
        self.buttonType = type
        super.init(frame: .zero)
        setupSubviews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        self.title = "test"
        self.type = LoadingButtonType.default.rawValue
        super.init(coder: aDecoder)
        setupSubviews()
    }
    
    /// MARK: public properties
    
    public func showError() {
        if !isLoading {
            return
        }
        isInErrorMode = true
        self.isLoading = false
    }
    
    public override var intrinsicContentSize: CGSize {
        let height: CGFloat = 40 + contentInset.top + contentInset.bottom
        if isLoading {
            return CGSize(width: height, height: height)
        }else{
            let sizeNeededForText = title.sizeInView(font: titleLabelFont)
            return CGSize(width: sizeNeededForText.width + contentInset.left + contentInset.right, height: height)
        }
    }
    
    /// MARK: touch methods
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        onTouchEntered()
        super.touchesBegan(touches, with: event)
    }
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        onTouchLeft()
        super.touchesEnded(touches, with: event)
    }
    
    public override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        onTouchLeft()
        super.touchesCancelled(touches, with: event)
    }
    
    /// we must call layout label since we are manually setting it's frame
    /// also since the animation is done when the setNeedsLayout is called
    // the animation should be performed in this method
    public override func layoutSubviews() {
        onIsLoadingChanged()
        layoutLabel()
        super.layoutSubviews()
    }
    
}


/**
 * private method
 **/
extension LoadingButton {
    
    fileprivate func setupSubviews() {
        setupSelf()
        setupLabel()
        setupLoadingView()
        onIsLoadingChanged()
    }
    
    /// color used as accent color for other porperties such as background, border and title
    fileprivate var accentColor: UIColor {
        if isInErrorMode {
            return UIColor(hex: "ff4d4d")
        }else{
            let newAccentColor = self.isEnabled || isLoading ? Styles.global.accentColor : Styles.global.defaultDisabledButtonColor
            return newAccentColor
        }
    }
    
    /// color for self.backgroundColor
    fileprivate var accentBackgroundColor: UIColor {
        switch buttonType {
        case .bordered:
            return .clear
        case .default:
            return accentColor
        case .minimal:
            return .clear
        }
    }
    
    /// color for self.borderColor
    fileprivate var accentBorderColor: UIColor {
        switch buttonType {
        case .bordered:
            return accentColor
        case .default:
            return .clear
        case .minimal:
            return .clear
        }
    }
    
    /// color for title label text
    fileprivate var accentTitleColor: UIColor {
        switch buttonType {
        case .bordered:
            return accentColor
        case .default:
            return .white
        case .minimal:
            return accentColor
        }
    }
    
    /// styles and configes the main view
    fileprivate func setupSelf() {
        backgroundColor = accentBackgroundColor
        layer.cornerRadius = Styles.global.defaultBorderRadius
        layer.borderColor = accentBorderColor.cgColor
        layer.borderWidth = 1
    }
    
    /// styles and configures the label
    fileprivate func setupLabel() {
        addSubview(label)
        label.textColor = accentTitleColor
        label.text = title
        label.font = Styles.global.fontNormal
        label.textAlignment = .center
        label.backgroundColor = .clear
    }
    
    /// setting the label for frame for label
    fileprivate func layoutLabel() {
        label.frame = CGRect(origin: .zero, size: self.bounds.size)
    }
    
    /// style and configures loading view
    fileprivate func setupLoadingView() {
        if loadingView != nil {
            loadingView.removeFromSuperview()
        }
        switch buttonType {
        case .default:
            loadingView = LoadingView(type: .small, color: .white)
        case .bordered:
            loadingView = LoadingView(type: .small, color: .accent(Styles.global.accentColor))
        case .minimal:
            loadingView = LoadingView(type: .small, color: .accent(Styles.global.accentColor))
        }
        loadingView.isHidden = true
        addSubview(loadingView)
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loadingView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    /// base method for performing animation when the isLoading changes
    fileprivate func onIsLoadingChanged() {
        if isLoading {
            self.label.isHidden = true
            self.loadingView.isHidden = false
            self.loadingView.alpha = 0
            UIView.animate(withDuration: 0.2) {
                self.loadingView.alpha = 1
            }
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.loadingView.alpha = 0
            }, completion: { (_) in
                self.label.isHidden = false
            })
        }
        switch buttonType {
        case .default:
            self.backgroundColor = self.accentColor
        case .bordered:
            self.label.textColor = self.accentColor
            self.layer.borderColor = self.accentColor.cgColor
        case .minimal:
            self.label.textColor = self.accentColor
        }
        if isInErrorMode {
            UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: UIViewKeyframeAnimationOptions.calculationModeCubicPaced, animations: {
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2, animations: {
                    self.transform = CGAffineTransform(translationX: 10, y: 0)
                })
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2, animations: {
                    self.transform = CGAffineTransform(translationX: -10, y: 0)
                })
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2, animations: {
                    self.transform = CGAffineTransform(translationX: 10, y: 0)
                })
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2, animations: {
                    self.transform = CGAffineTransform(translationX: -10, y: 0)
                })
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2, animations: {
                    self.transform = CGAffineTransform(translationX: 0, y: 0)
                })
            })
        }
        self.isInErrorMode = false
    }
    
    /// will cause the layout to be animated
    fileprivate func animateStyleChange() {
        UIView.animate(withDuration: 0.2) {
            self.invalidateIntrinsicContentSize()
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }
    
    /// changes for when the touch enters the button
    fileprivate func onTouchEntered() {
        label.alpha = 0.35
    }
    
    /// changes for when the touched leaves or cancels
    fileprivate func onTouchLeft() {
        label.alpha = 1
    }
    
}


