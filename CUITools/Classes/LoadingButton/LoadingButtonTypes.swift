//
//  LoadingButtonTypes.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 8/22/1396 AP.
//

import Foundation

/**
 * defines the types for loading button
 * the raw value should be used for type property of LoadingButton
 * since the enum cannot be represented in objc
 */
public enum LoadingButtonType: Int {
    case `default` = 0
    case bordered = 1
    case minimal = 2
}
