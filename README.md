# CUITools

[![CI Status](http://img.shields.io/travis/mohsenShakiba/CUITools.svg?style=flat)](https://travis-ci.org/mohsenShakiba/CUITools)
[![Version](https://img.shields.io/cocoapods/v/CUITools.svg?style=flat)](http://cocoapods.org/pods/CUITools)
[![License](https://img.shields.io/cocoapods/l/CUITools.svg?style=flat)](http://cocoapods.org/pods/CUITools)
[![Platform](https://img.shields.io/cocoapods/p/CUITools.svg?style=flat)](http://cocoapods.org/pods/CUITools)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CUITools is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CUITools'
```

## Author

mohsenShakiba, work.shakiba@gmail.com

## License

CUITools is available under the MIT license. See the LICENSE file for more info.
